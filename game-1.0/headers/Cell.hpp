
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_CELL_HEADER
#define GAME1_CELL_HEADER

#include "Sprite_Node.hpp"
#include "String.hpp"

#include "Tools.hpp"

namespace game1
{
	using utils_1::Sprite_Node;
	using toolkit::String;

	class Cell : public Sprite_Node
	{

		//Propiedades p�blicas
	public:

		//Los estados de la celda
		enum CellStates { Free, HighLighted, Occupied } cellState;

		//Guardamos una referencia a la tool que se situa sobre ella
		Tools * tool;

		//El id de la celda ( la columna a la que pertenece la celda )
		size_t id_asSize_t;

		//Propiedades privadas
	private:

		//El slice para cuando no est� seleccionada
		Image::Slice * normalSlice;

		//El slice para cuando hay que resaltar la celda
		Image::Slice * highLightedSlice;

		//M�todos p�blicos
	public:

		//Constructor
		Cell( const size_t _cellID, const float _width, const float _height, const float _posX, const float _posY );
		
		//M�todo que cambia el estado de la celda
		void changeCellState( CellStates newCellState );

	};
}

#endif