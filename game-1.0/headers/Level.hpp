
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						19 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_LEVEL_HEADER
#define GAME1_LEVEL_HEADER

//#include <vector>
#include <Shared_Ptr.hpp>

#include <Group.hpp>
#include <Transformable.hpp>

#include "Board.hpp"

namespace game1
{

	//using std::vector;
	using toolkit::Shared_Ptr;

	class Level
	{
		// Vector de posici�n Inicial del Nivel ( Entrada )
		//Vector2f initialPosition;

		// Vector de posicion Final del Nivel ( Salida )
		//Vector2f endPosition;

	//Propiedades p�blicas
	public:

		// Referencia al tablero
		Shared_Ptr<Board> board;

		//M�todos p�blicos
	public:

		//Constructor por defecto
		Level()
		{
		}

	};
}

#endif