
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_GAMEPLAY_HEADER
#define GAME1_GAMEPLAY_HEADER

#include "UI.hpp"
#include "Board.hpp"
//#include "Player.hpp"
#include "Player_Camera.hpp"
#include "Scene.hpp"
#include "Level.hpp"
#include "Image.hpp"
#include "GamePlayUI.hpp"
#include "ToolsLayer.hpp"
#include "GameOver.hpp"

namespace game1
{

	using snack::Scene;
	using snack::Image;
	using snack::Rasterizer;
	using snack::Pointer_Event;

	class Player;

	class Game1Gameplay : public tribalAction::UI
	{

		//Propiedades p�blicas
	public:
		
		//La textura 1 con las im�genes para las tools
		Shared_Ptr<Image> tools_sheet_1;

		//La interfaz gr�fica del juego
		Shared_Ptr<GamePlayUI> gamePlayUI;

		//La capa para las tools que maneja el player
		Shared_Ptr<ToolsLayer> toolsLayer;

		//Referencia al nivel
		Shared_Ptr<Level> level;

		//El player
		Shared_Ptr<Player> player;

		//La c�mara que sigue al player
		Shared_Ptr<Player_Camera> playerCamera;

		//Propiedades privadas
	private:

		//Estados del GamePlay
		enum GameMainStates { LOADING ,PLANNING, EXECUTION } gameMainState;

		//El grafo de escena
		Shared_Ptr<Scene> gamePlayScene;

		//Estados del Modo Planificacion
		enum GamePlanificationStates { Melon, Melon2 } gamePlanificationState;

		//Estados del Modo Ejecuci�n
		enum GameExecutionStates { Sandia, Sandia2 } gameExecutionState;

		//La capa para el GameOver
		Shared_Ptr<GameOver> gameOverLayer;

		//M�todos p�blicos
	public:
		
		//M�todo que comienza el estado de ejecuci�n ( una vez pulsado "Play" )
		void execute();

		//M�todo que se lanza cuando el player llega a la meta
		void endAchieve()
		{

		}

		//M�todo que se lanza cuando el player llega a la meta
		void gameOver()
		{
		}

		//M�todos privados
	private:

		//M�todo que virtuales de UI que se sobreescriben
		void initialize         ();
        //virtual void pause              ();
        //virtual void resume             ();
        //virtual void finalize           ();
        void halt               ();

        void update             (float  real_frame_delta);
        void render             (Rasterizer & rasterizer);

        //virtual void got_focus          ();
        //virtual void size_changed       ();

        void pointer_pressed    (const Pointer_Event & event);
        void pointer_dragged    (const Pointer_Event & event);
        void pointer_released   (const Pointer_Event & event);

		// Resetea el Nivel
		void resetLevel();
	};
}
#endif