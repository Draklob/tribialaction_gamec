
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_PLAYER_HEADER
#define GAME1_PLAYER_HEADER

#include "Sprite_Node.hpp"
#include "String.hpp"

namespace game1
{
	using utils_1::Sprite_Node;
	using toolkit::String;

	class Player : public Sprite_Node
	{
		
		//Propiedades p�blicas
	public:

		//Los estados del player
		enum PlayerStates { Inactive, Moving, Evaluating, Dead } playerState;

		//Los estados de la direcci�n player para moverlo y pintarlo correctamente
		enum PlayerDirection { LEFT, UP, RIGHT, DOWN } playerDirection;

		//Propiedades privadas
	private:

		//El slice para el player para el estado de muerto
		//Igual tendr�a que haber heredado de SimpleButton_Node que tiene los dos estados
		Image::Slice * deadSlice;

		//El slice para la izquierda
		Image::Slice * leftSlice;

		//El slice para arriba
		Image::Slice * upSlice;

		//El slice para la derecha
		Image::Slice * rightSlice;

		//El slice para abajo
		Image::Slice * downSlice;

		//La mitad de la anchura y de la altura para luego poder calcular din�micamente el centro de la tool
		Vector2f halfSize;

		//El tiempo que tarda en ir de una celda a otra
		float totalTransitionTime;

		//El tiempo que lleva transcurrido en un trayecto concreto
		float elapsedTransitionTime;

		//Distancia que tiene que recorrer para ir a la siguiente celda
		float cellsDistance;

		//La posici�n inicial en cada trayecto ( para poder ir calculando el incremento )
		Vector2f initialPosition;

		//M�todos p�blicos
	public:

		//El constructor por defecto
		Player()
		:
			Sprite_Node(), playerState( Inactive ), playerDirection( UP ),
			totalTransitionTime( 1.f ), elapsedTransitionTime( 0.f )
		{
			//Inicialmente lo ponemos a invisible
			visible = false;
		}

		//M�todo que configura el player cuando le damos a ejecutar
		void setPlayer( const float _posX, const float _posY, const float _cellWidth );

		//M�todo que cambia la direcci�n y la textura de la casilla
		void changeDirecction( PlayerDirection newPlayerDirection );

		//M�todo que acciona el player para que comience a moverse
		void activatePlayer()
		{
			visible = true;
			playerState = Moving;
		}

		//Actualiza la posici�n del player cuando el juego est� en modo EXECUTION
		void update	(float real_frame_delta);

		//M�todos privados
	private:

		// Eval�a las posibilidades para el player en funci�n de la celda en la que se encuentre
		// y la direcci�n que lleve actualmente
		void evaluatePossibilitiesTree();

		// M�todo que reseta las propiedades para la transici�n del player
		void resetTransitionProperties();

		// Devuelve la posici�n del player pasado un tiempo determinado
		Vector2f calculatePlayerPosition( const float elpasedTime );


	};
}

#endif