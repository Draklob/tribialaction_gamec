
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_PLAYER_CAMERA_HEADER
#define GAME1_PLAYER_CAMERA_HEADER


#include <Group.hpp>
#include <Transformable.hpp>

namespace game1
{
	using snack::Group;
	using snack::Transformable;

	class Player_Camera : public Group, public Transformable
	{

	};
}

#endif