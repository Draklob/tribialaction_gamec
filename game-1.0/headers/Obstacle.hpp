
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_OBSTACLE_HEADER
#define GAME1_OBSTACLE_HEADER

#include "Tools.hpp"

#include "SingletonManagers.hpp"

#include "Vector.hpp"

#include <String.hpp>

namespace game1
{
	using tribalAction::SingletonManagers;
	using toolkit::Vector2f;
	using toolkit::String;

	class Obstacle : public Tools
	{
		//Propiedades p�blicas
	public:

		//Como altera la direcci�n del player
		Vector2f playerDirectionChange;

		//M�todos p�blicos
	public:
		//Constructor 
		Obstacle( const String & _id, const ToolType _toolType )
		:
			Tools( _id, _toolType )
		{
			setObstacle();
		}

		//Constructor pas�ndole una posici�n en el mapa
		Obstacle( const String & _id, const ToolType _toolType, const float _posX, const float _posY )
		:
			Tools( _id, _toolType )
		{
			setObstacle();
			set_location( _posX, _posY );
		}

		//Act�a sobre el player para modificar sus propiedades
		void changePlayerPropierties();

		//M�todos privados
	private:

		//Constructor privado
		void setObstacle();
		
	};
}

#endif