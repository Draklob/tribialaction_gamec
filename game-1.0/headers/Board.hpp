
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_BOARD_HEADER
#define GAME1_BOARD_HEADER


#include <Group.hpp>
#include <Transformable.hpp>

//Para poder convertir enteros a string para el score por ejemplo
#include <sstream>

#include "BoardLayer.hpp"

namespace game1
{
	using std::string;
	using snack::Group;
	using snack::Transformable;

	//Declaraci�n anticipada de Cell porque si no da conflicto
	class Cell;
	class Tools;
	
	class Board : public Group, public Transformable
	{
		
		//Propiedades p�blicas
	public:

		//N�mero de celdas 
		size_t colums_number;
		size_t rows_number;

		//El tama�o del mapa
		float width;
		float height;

		//El tama�o de cada celda
		float cellWidth;
		float cellHeight;

		//La capa que contendr� las celdas base del tablero
		Shared_Ptr<BoardLayer> cellsLayer;

		//La capa que contendr� las tools que colocamos en el tablero
		Shared_Ptr<BoardLayer> boardTools;

		//Referencia a la celda resaltada
		Cell * highlightedCell;

		//Propiedades privadas
	private:

	
		//M�todos p�blicos
	public:

		//Constructor por defecto
		Board();

		//M�todo que carga el mapa dependiendo del path que le pasemos por par�metro
		void loadBoard( string _boardPath );

		//M�todo que centra el mapa a lo ancho y alto ( teniendo en cuenta el espacio que ocupa el hud )
		void centerBoard();

		//M�todo que chequea si hay colisi�n de una tool con alguna celda del tablero
		//para cuando vamos arrastrando la tool se vayan iluminando las celdas por las que pasamos
		void showOnCell( Vector2f _toolPositioon );

		//M�todo que deselecciona la celda que estaba highlighted
		void showOffCell();

		//M�todo que pone como ocupada la celda que estaba seleccionada como highlighted
		void highLightedToOccupied();

		//Devuelve la posici�n de la celda seleccionada ( highlightedCell )
		Vector2f gethighlightedCellPosition();

		//M�todo que inserta una tool en la capa correspondiente ( toolsLayer )
		//void instertIntoToolsLayer( const Tools & newTool );

		//M�todo que devuelve el tipo de tool sobre una celda pas�ndole una posici�n
		//Si la celda est� libre y no tiene ninguna tool, entonces devuelve None
		Tools * getToolTypeOverCell( Vector2f position );

		//M�todos privados
	private:

		//Devuelve la celda que est� en la fila y la columna pasadas por par�metro
		Cell* selectCell( size_t row, size_t col ) const;

		//Convierte coordenadas absolutas a relativas al mapa ( teniendo en cuenta el movimiento de la c�mara )
		Vector2f absolutesToRelatives( Vector2f absolutePosition );

		//Convierte coordenadas relativas al mapa a absolutas ( teniendo en cuenta el movimiento de la c�mara )
		Vector2f relativesToAbsolutes( Vector2f relativePosition );

		//Convierte una posici�n de pixels a coordenadas de tablero ( row y col )
		Vector2f pixelsToGrid( Vector2f pixelsPosition );

		//Convierte una posici�n de coordenadas de tablero ( row y col ) a pixels
		Vector2f gridToPixels( Vector2f gridPosition );

	};
}

#endif