
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						19 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_GAMEPLAY_UI_HEADER
#define GAME1_GAMEPLAY_UI_HEADER


#include <Group.hpp>
#include <Transformable.hpp>
#include <Image.hpp>


namespace game1
{
	using snack::Group;
	using snack::Transformable;
	using snack::Image;
	
	class GamePlayUI : public Group, public Transformable
	{

		//Propiedades p�blicas
	public:

		//La dimensi�n de la interfaz
		float width;
		float height;

		//La coordenada en y ( para saber si hacemos click en la zona del hud o por encima )
		float coordY;

		//La coordenada en x del extremo del hud de la izquierda, para optimizar el c�lculo
		//de si hacemos click en la zona derecha del hud( tools ) o en la zona de la izquierda ( botones de play, men� y zoom )
		float rightBorder;

		//Declaramos una clase grupo para incluir los botones
		class ButtonsGroup : public Group, public Transformable
		{
		};

		Shared_Ptr<ButtonsGroup> buttonsGroup;

		//M�todos p�blicos
	public:

		//Constructor 
		GamePlayUI( float _width, float _height )
		:	width(_width), height(_height)
		{
		}

		//Inicializa la UI
		void initialize();

		//Le pasamos el evento pressed cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
		void pointer_pressed (const Vector2f & pointerPosition );

		//Le pasamos el evento released cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
		void pointer_released (const Vector2f & pointerPosition );


	};
}

#endif