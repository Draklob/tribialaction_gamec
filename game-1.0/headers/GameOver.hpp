
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						20 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_GAMEOVER_HEADER
#define GAME1_GAMEOVER_HEADER


#include <Group.hpp>
#include <Transformable.hpp>
#include <Image.hpp>

namespace game1
{
	using snack::Group;
	using snack::Transformable;
	using snack::Image;
	
	class GameOver : public Group, public Transformable
	{

		//Propiedades p�blicas
	public:

		//La dimensi�n de la pantalla 
		float width;
		float height;

		//La posici�n inicial ( por si queremos luego animarla )
		Vector2f initialPosition;

		//Declaramos una clase grupo para incluir los botones
		class ButtonsGroup : public Group, public Transformable
		{
		};

		Shared_Ptr<ButtonsGroup> buttonsGroup;

		//M�todos p�blicos
	public:

		//Constructor 
		GameOver( Vector2f _initialPosition, float _width, float _height )
		:	initialPosition( _initialPosition), width(_width), height(_height)
		{
		}

		//Inicializa la UI
		void initialize();

		//Le pasamos el evento pressed cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
		void pointer_pressed (const Vector2f & pointerPosition );

		//Le pasamos el evento released cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
		void pointer_released (const Vector2f & pointerPosition );


	};
}

#endif