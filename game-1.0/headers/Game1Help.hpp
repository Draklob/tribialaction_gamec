
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_HELP_HEADER
#define GAME1_HELP_HEADER
	
#include "UI.hpp"

#include <vector>
#include "Button_Node.hpp"

#include <Pointer_Event.hpp>
#include <Group.hpp>
#include <Scene.hpp>
#include <Transformable.hpp>
#include "Image.hpp"

#include "Sprite_Node.hpp"

namespace game1
{
	using std::vector;
	using utils_1::Button_Node;

	using utils_1::Sprite_Node;

	using snack::Scene;
	using snack::Pointer_Event;
	using snack::Group;
	using snack::Transformable;
	using snack::Rasterizer;
	using snack::Image;

	class Game1Help : public tribalAction::UI
	{

		//Propiedades p�blicas
	public:

		//Declaramos una clase grupo para los botones
		class MenuBtns_Group : public Group, public Transformable
		{
		};

		//Propiedades privadas
	private:

		//La escena para el tutorial
		Shared_Ptr<Scene> tutorialScene;

		//La 1� imagen del tutorial
		Shared_Ptr<Sprite_Node> image1;

		//La 2� imagen del tutorial
		Shared_Ptr<Sprite_Node> image2;

		//La 3� imagen del tutorial
		Shared_Ptr<Sprite_Node> image3;

		// Identificador de la imagen que ense�amos de tutorial
		int imgTutorial;

		//El grupo de botones
		Shared_Ptr<MenuBtns_Group> tutorialBtns_group;

		//Si es visible
		bool is_visible;

		//M�todos privados
	private:

		//M�todo que virtuales de UI que se sobreescriben
		void initialize();
		//virtual void pause              () { }
		//virtual void resume             () { }
		//virtual void finalize           () { }
		void halt();

		//virtual void update             (float  real_frame_delta) { }
		void render(Rasterizer & rasterizer);

		//virtual void got_focus          () { }
		//virtual void size_changed       () { }

		virtual void pointer_pressed(const Pointer_Event & event);
		//virtual void pointer_dragged    (const Pointer_Event & event);
		virtual void pointer_released(const Pointer_Event & event);

	};
}

#endif