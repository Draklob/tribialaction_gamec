
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						19 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_START_HEADER
#define GAME1_START_HEADER

#include "Tools.hpp"

#include "SingletonManagers.hpp"

#include "Vector.hpp"

#include <String.hpp>

namespace game1
{
	using tribalAction::SingletonManagers;
	using toolkit::Vector2f;
	using toolkit::String;

	class Start : public Tools
	{
		//Propiedades p�blicas
	public:

		//Los diferentes estados que puede tener
		enum StartStates{ LEFT, UP, RIGHT, DOWN } startState;

		//Como altera la direcci�n del player
		Vector2f playerDirectionChange;

		//Propiedades privadas
	private:

		//El slice para la izquierda
		Image::Slice * leftSlice;

		//El slice para arriba
		Image::Slice * upSlice;

		//El slice para la derecha
		Image::Slice * rightSlice;

		//El slice para abajo
		Image::Slice * downSlice;

		//M�todos p�blicos
	public:
		//Constructor 
		Start( const String & _id, const ToolType _toolType, const StartStates _startStates )
		:
			Tools( _id, _toolType ), startState( _startStates )
		{
			setStart();
		}

		//Constructor pas�ndole una posici�n en el mapa
		Start( const String & _id, const ToolType _toolType, const StartStates _startStates, const float _posX, const float _posY )
		:
			Tools( _id, _toolType ), startState( _startStates )
		{
			set_location( _posX, _posY );
			setStart();
		}

		//Act�a sobre el player para modificar sus propiedades
		void changePlayerPropierties();

		//M�todos privados
	private:

		//M�todo que carga las texturas correspondientes para las cuatro direcciones
		void setStart();

		//M�todo que cambia la direcci�n y la textura de la casilla
		void changeDirecction();
		
	};
}

#endif