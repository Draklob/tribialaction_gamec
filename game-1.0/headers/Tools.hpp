
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_TOOLS_HEADER
#define GAME1_TOOLS_HEADER

#include "SimpleButton_Node.hpp"

#include <String.hpp>

namespace game1
{
	using utils_1::SimpleButton_Node;
	using toolkit::String;

	class Tools : public SimpleButton_Node
	{
		//Propiedades p�blicas
	public:

		//Los tipos de tools disponibles
		enum ToolType { None, Arrow, Wall, Obstacle, Start, End } toolType; 

		//La posici�n inicial de la tool en el hud
		Vector2f initialPosition;

		//Propiedades privadas
	protected:

		//Si ha sido situada ya en el tablero
		bool located;

		//La mitad de la anchura y de la altura para luego poder calcular din�micamente el centro de la tool
		Vector2f halfSize;

		//M�todos p�blicos
	public:

		//Constructor en el que le pasamos un id
		Tools( const String & _id, const ToolType _toolType )
		: 
			SimpleButton_Node( _id ), toolType( _toolType ) ,located( false )
		{
			//Para que se pinte el slice y no la sheet
			is_slice = true;
		}

		//Constructor para la tool End
		//Constructor en el que le pasamos un id, una referencia al sheet y el nombre del slice 
		Tools( const string & _id, const ToolType _toolType, const Shared_Ptr< Image > sheet, const string slice_name,
																						float posX, float posY )
		:
			SimpleButton_Node( _id, sheet, slice_name, slice_name ), toolType( _toolType )
		{
			set_location( posX, posY );
		}

		//Calcula el centro de la tool a partir de la posici�n del extremo superior izquierdo
		Vector2f getCenter( Vector2f newPosition ) const
		{
			return Vector2f( newPosition.x() + halfSize.x(), newPosition.y() + halfSize.y() );
		}

		//Devuelve la tool a la posici�n inicial en el hud
		void resetInitialPosition()
		{
			set_location( initialPosition );
		}

		//M�todo que centra la tool respecto a una posici�n de celda
		void setCenteredIntoCell( const Vector2f & cellPosition )
		{
			set_location( cellPosition );
		}

		virtual void changePlayerPropierties();

	
	};
}

#endif