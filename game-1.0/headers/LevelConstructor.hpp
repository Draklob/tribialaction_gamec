
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						19 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */


#ifndef GAME1_LEVELCONSTRUCTOR_HEADER
#define GAME1_LEVELCONSTRUCTOR_HEADER

#include "Level.hpp"

	namespace game1
	{
		//M�todo que construye el nivel pasado por par�metro
		Shared_Ptr<Level> constructLevel( size_t levelNumber );
	}

#endif // 