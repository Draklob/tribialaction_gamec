
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_MANAGER_HEADER
#define GAME1_MANAGER_HEADER

	#include <View.hpp>
	//Para poder cargar im�genes
	#include "Image.hpp"

	#include <string>

    namespace game1
    {

		//Incluimos una declaraci�n adelantada de las clases que instanciamos desde
		//el gestor principal del juego
		class Game1Menu;
		class Game1Help;
		class Game1Gameplay;

		using snack::View;
		using snack::Image;
		
		class Game1Manager : public View
		{
			//Propiedades p�blicas
		public:

			//Estados del juego Game1
			enum Game1States { MENU, GAMEPLAY, TUTORIAL, CREDITS, EXIT } game1State;

			//La textura 1 con im�genes para la interfaz de usuario ( hud )
			Shared_Ptr<Image> ui_sheet_1;

			//La textura 1 con im�genes para la interfaz de usuario ( menu )
			Shared_Ptr<Image> menu_sheet;

			// Nivel en el que est� el player
			size_t levelNum;

			//Propiedades privadas
        private:

			//Puntero a la intro
			Shared_Ptr< Game1Menu > game1Menu;

			// Puntero a Gameplay
			Shared_Ptr<Game1Gameplay> game1Gameplay;

			//Referencia al gameManager del namespace game1
            Shared_Ptr<Game1Help> game1Help;

			//M�todos p�blicos
		public:

			//M�todo que cambia el estado de la aplicaci�n
			void changeState( const Game1States newState );

			//M�todos privados
		private:

			//M�todo que virtuales de View que se sobreescriben
            void initialize	();
			void halt		();

			//M�todos que lanzan las diferentes escemas del juego Game1
			void start_Menu();
			void start_Gameplay();
			void start_Help();
			
		};
	}

#endif