
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_TOOLS_LAYER_HEADER
#define GAME1_TOOLS_LAYER_HEADER


#include <Group.hpp>
#include <Transformable.hpp>

namespace game1
{
	using snack::Group;
	using snack::Transformable;

	class ToolsLayer : public Group, public Transformable
	{

		//Propiedades p�blicas
	public:

		//Estados para saber si estamos arrastrando o no una tool
		enum ToolsState { Ready, Dragging, AllLocated } toolsState;

		//El n�mero de tools restantes que quedan por situar en el tablero
		size_t toolsStillInHud;

		//Propiedades privadas
	private:

		//La posici�n inicial del primer tool del slider de tools
		Vector2f toolsInitialPosition;

		//La distancia en horizontal entre cada tools dentro del hud
		float toolsDistance;

		//La distancia entre el puntero y la posici�n del objeto al hacer click 
		//para hacer un correcto dragging
		Vector2f relativeDistance;

		//M�todos p�blicos
	public:

		//El constructor por defecto
        ToolsLayer();

		//Posiciona las tools
		void resetToolsPositions();

		//Le pasamos el evento pressed cuando hemos pulsado en la zona derecha del hud ( la de las tools )
		void pointer_pressed (const Vector2f & pointerPosition );

		//Le pasamos el evento dragged cuando estamos arrastrando en el estado de PLANNING
		void pointer_dragged (const Vector2f & pointerPosition );

		//Le pasamos el evento released cuando hemos pulsado en la zona derecha del hud ( la de las tools )
		void pointer_released (const Vector2f & pointerPosition );

	};
}

#endif