

/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_BOARD_LAYER_HEADER
#define GAME1_BOARD_LAYER_HEADER


#include <Group.hpp>
#include <Transformable.hpp>

#include "String.hpp"

namespace game1
{
	using snack::Group;
	using snack::Transformable;
	using toolkit::String;

	class BoardLayer : public Group, public Transformable
	{

		//Propiedades p�blicas
	public:

		//Declaramos una clase grupo para incluir las diferentes filas de celdas de la capa
		//Cada capa tendr� un id que indicar� qu� fila es para optimizar la b�squeda de colisiones en el mapa
		class Row : public Group, public Transformable
		{
			//Propiedades p�blicas
		public:

			//El id de la fila ( el n�mero de fila )
			size_t id_asSize_t;

		public:
			Row( const size_t & given_id = size_t() )
            :
                id_asSize_t(given_id)
			{
			}
		};

		//M�todos p�blicos
	public:

		//El constructor 
        BoardLayer()
		{
		}

	};
}

#endif