
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef GAME1_ARROW_HEADER
#define GAME1_ARROW_HEADER

#include "Tools.hpp"

#include "SingletonManagers.hpp"

#include "Vector.hpp"

#include <String.hpp>

namespace game1
{
	using tribalAction::SingletonManagers;
	using toolkit::Vector2f;
	using toolkit::String;

	class Arrow : public Tools
	{
		//Propiedades p�blicas
	public:

		//Estados del juego Game1
		enum ARROWDIRECTIONS { LEFT, UP, RIGHT, DOWN } arrowDirection;

		//M�todos p�blicos
	public:

		//Constructor 
		Arrow( const String _id, const ToolType _toolType, const ARROWDIRECTIONS _arrowDirection )
		: 
			Tools( _id, _toolType ), arrowDirection( _arrowDirection )
		{
			setArrow( _arrowDirection );	
		}

		//Constructor pas�ndole una posici�n en el mapa
		Arrow( const String & _id, const ToolType _toolType, const ARROWDIRECTIONS _arrowDirection, const float _posX, const float _posY )
		:	
			Tools( _id, _toolType ), arrowDirection( _arrowDirection )
		{
			setArrow( _arrowDirection );
			set_location( _posX, _posX );
		}

		//Act�a sobre el player para modificar sus propiedades
		void changePlayerPropierties();

		//M�todos privados
	private:

		//Constructor privado
		void setArrow( ARROWDIRECTIONS _arrowDirection );
		
	};
}

#endif