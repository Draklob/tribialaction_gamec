
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Board.hpp"

#include "BoardLayer.hpp"

#include <iostream>
#include <sstream>

#include <iostream>
#include <sstream>

#include <rapidxml.hpp>
#include "File_Loader.hpp"

#include "SingletonManagers.hpp"

#include "Cell.hpp"

#include "Utils.hpp"

//Las diferentes tools
#include "Tools.hpp"
#include "Arrow.hpp"
#include "Wall.hpp"
#include "Obstacle.hpp"
#include "Start.hpp"

//El player
#include "Player.hpp"

//#include "Image.hpp"

using namespace std;
using namespace snack;
using namespace rapidxml;
using namespace tribalAction;
using namespace utils_1;


namespace game1
{
	//El constructor
	Board::Board()
	{
		//Calculamos el tama�o de las celdas 
		if( SingletonManagers::getInstance().app_main_manager->deviceType == SingletonManagers::getInstance().app_main_manager->HIGH )
		{
			cellHeight = cellWidth = 135 * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		}
		else if( SingletonManagers::getInstance().app_main_manager->deviceType == SingletonManagers::getInstance().app_main_manager->MEDIUM )
		{
			cellHeight = cellWidth = 90 * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		}
		else if( SingletonManagers::getInstance().app_main_manager->deviceType == SingletonManagers::getInstance().app_main_manager->LOW )
		{
			cellHeight = cellWidth = 40 * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		}

		// Creamos la capa para las celdas y la agregamos al grafo
		// ( En este mapa cargamos tan solo un layer )
		cellsLayer = Shared_Ptr<BoardLayer>( new BoardLayer() );
		this->push_back( cellsLayer );

		//Inicializamos la capa para las tools del Board
		boardTools = Shared_Ptr<BoardLayer>( new BoardLayer() );
		//La agregamos al grafo de escena
		this->push_back( boardTools );

		//Inicializamos el puntero a la celda seleccionada
		highlightedCell = null;
	}

	//M�todo que centra el mapa a lo ancho y alto ( teniendo en cuenta el espacio que ocupa el hud )
	void Board::centerBoard( )
	{
		//Calculamos la diferencia de tama�o entre la pantalla y el tablero
		float diferenceX = SingletonManagers::getInstance().app_main_manager->screenSizef.x() - width;
		//En el eje Y hay que tener en cuenta el espacio que ocupa la interfaz ( HUD )
		float diferenceY = SingletonManagers::getInstance().app_main_manager->screenSizef.y() -  SingletonManagers::getInstance().game1Gameplay->gamePlayUI->height - height;
		//Calculamos el margen respecto a los bordes para centrar el tablero
		float marginX = diferenceX / 2;
		float marginY = diferenceY / 2;
		//Movemos el tablero para centrarlo
		Vector2f displacement = Vector2f( marginX, marginY );
		//Movemos la c�mara para centrar el tablero
		SingletonManagers::getInstance().game1Gameplay->playerCamera->move( displacement );
	}

	//M�todo que devuelve el tipo de tool sobre una celda pas�ndole una posici�n
	//Si la celda est� libre y no tiene ninguna tool registrada, entonces devuelve null
	Tools * Board::getToolTypeOverCell( Vector2f position )
	{
		Vector2f toolRelativePosition = absolutesToRelatives( position );
		size_t row = (size_t)( toolRelativePosition.y() / cellHeight );
		size_t col = (size_t)( toolRelativePosition.x() / cellWidth );

		//Seleccionamos la celda con esas coordenadas
		Cell * cell = selectCell( row, col );
		
		//Devolvemos el puntero a tool de la celda
		return cell->tool;
	}

	//M�todo que chequea si hay colisi�n de una tool con alguna celda del tablero
	//para cuando vamos arrastrando la tool se vayan iluminando las celdas por las que pasamos
	void Board::showOnCell( Vector2f _toolPositioon )
	{
		Vector2f toolRelativePosition = absolutesToRelatives( _toolPositioon );
		size_t row = (size_t)( toolRelativePosition.y() / cellHeight );
		size_t col = (size_t)( toolRelativePosition.x() / cellWidth );

		if( row >= 0 && row < rows_number && col >= 0 && col < colums_number )
		{
			Cell * cell = selectCell( row, col );
			//Primero ponemos en modo normal la anteriormente resaltada
			//y ponemos a null el puntero donde guardamos referencia a la misma
			if( highlightedCell ) showOffCell();
			
			//La celda devuelta la ponemos como resaltada
			if( cell != null )
			{
				if( cell->cellState == Cell::Free )
				{
					//Ponemos la nueva celda como seleccionada
					cell->changeCellState( Cell::HighLighted );
					//Y la guardamos en la referencia del mapa 
					highlightedCell =  cell;
				}
			}
		}
	}

	//M�todo que deselecciona la celda que estaba highlighted
	void Board::showOffCell()
	{
		if( highlightedCell )
		{
			highlightedCell->changeCellState( Cell::Free );
			highlightedCell = null;
		}
	}

	//Devuelve la posici�n de la celda seleccionada ( highlightedCell )
	Vector2f Board::gethighlightedCellPosition()
	{
		return relativesToAbsolutes( highlightedCell->get_location() );
	}

	//M�todo que pone como ocupada la celda que estaba seleccionada como highlighted
	void Board::highLightedToOccupied()
	{
		if( highlightedCell )
		{
			highlightedCell->changeCellState( Cell::Occupied );
			highlightedCell = null;
		}
	}

	//M�todo que inserta una tool en la capa correspondiente ( toolsLayer )
	/*void Board::instertIntoToolsLayer( const Tools & newTool )
	{
		//Shared_Ptr<Tools>tool = Shared_Ptr<Tools>( new Tools() );
		//SingletonManagers::getInstance().game1Gameplay->level->board->boardTools->push_back( tool );
	}*/

	//Devuelve la celda que est� en la fila y la columna pasadas por par�metro
	Cell* Board::selectCell( size_t row, size_t col ) const
	{
		//cout<< "cellsLayer_size_cellsLayer : " << cellsLayer->size() << endl;
		//Recorremos todas las filas hasta llegar a la indicada por row
		BoardLayer::Iterator it1;
		for (it1 = cellsLayer->begin(); it1 != cellsLayer->end(); ++it1)
		{
			BoardLayer::Row *ptr = (dynamic_cast<BoardLayer::Row*>(it1->get()));
			//cout<< "row_index : " << ptr->id_asSize_t << endl;
			//cout<< "row_size : " << ptr->size() << endl;
			if ( ptr->id_asSize_t == row )
			{
				//Recorremos las celdas para encontrar la que est� en la columna indicada
				BoardLayer::Row::Iterator it2;
				for (it2 = ptr->begin(); it2 != ptr->end(); ++it2)
				{
					Cell *ptr = (dynamic_cast<Cell*>(it2->get()));
					if ( ptr->id_asSize_t == col )
					{
						//La devolvemos
						return ptr;
						break;
					}
				}
				
				break;
			}
		}
		return null;
	}

	//Convierte coordenadas absolutas a relativas al mapa ( teniendo en cuenta el movimiento de la c�mara )
	Vector2f Board::absolutesToRelatives( Vector2f absolutePosition )
	{
		//Hay que restarle lo que se haya movido la c�mara
		float relativeCellPosition_X = absolutePosition.x() - SingletonManagers::getInstance().game1Gameplay->playerCamera->get_location_x();
		float relativeCellPosition_Y = absolutePosition.y() - SingletonManagers::getInstance().game1Gameplay->playerCamera->get_location_y();
		return Vector2f( relativeCellPosition_X, relativeCellPosition_Y  );
	}

	//Convierte coordenadas relativas al mapa a absolutas ( teniendo en cuenta el movimiento de la c�mara )
	Vector2f Board::relativesToAbsolutes( Vector2f relativePosition )
	{
		//Hay que sumarle lo que se haya movido la c�mara
		float absoluteCellPosition_X = relativePosition.x() + SingletonManagers::getInstance().game1Gameplay->playerCamera->get_location_x();
		float absoluteCellPosition_Y = relativePosition.y() + SingletonManagers::getInstance().game1Gameplay->playerCamera->get_location_y();
		return Vector2f( absoluteCellPosition_X, absoluteCellPosition_Y  );
	}

	//Convierte una posici�n de pixels a coordenadas de tablero ( row y col )
	Vector2f Board::pixelsToGrid( Vector2f pixelsPosition )
	{
		//De momento no est� implementado
		//Tendr�a que devolver un vector de size_t
		return Vector2f( 0.f, 0.f );
	}

	//Convierte una posici�n de coordenadas de tablero ( row y col ) a pixels
	Vector2f Board::gridToPixels( Vector2f gridPosition )
	{
		//De momento no est� implementado
		//Tendr�a que devolver un vector de size_t
		return Vector2f( 0.f, 0.f );
	}

	//M�todo que carga el mapa dependiendo del path que le pasemos por par�metro
	void Board::loadBoard( string _boardPath )
	{
		//Declaramos un objeto de la librer�a rapidxml de tipo xml_document
		xml_document<> doc;

		//Abrimos el archivo xml con los datos del mapa de tiles
		std::ifstream file( _boardPath.c_str() );
		
		//Lo convertimos a string
		std::stringstream buffer;
		buffer << file.rdbuf();
		file.close();
		std::string content(buffer.str());

		//Craemos el objeto doc a partir de este string
		doc.parse<0>(&content[0]);

		//Variables para recorrer el �rbol de nodos
		xml_node<> *pMap_Node; 
		xml_node<> *pLayer_Node;
		xml_node<> *pTile_Node;
		xml_attribute<> *pAttr;
		string cadena;
		float pos_x;
		float pos_y;
		
		//Parseamos los datos generales del mapa obtenidos del nodo raiz ( map )
		pMap_Node = doc.first_node();
		//Obtenemos el n�mero de columnas y el n�mero de filas del mapa
		cadena = pMap_Node->first_attribute("width")->value();
		colums_number = atoi(cadena.c_str()); 
		cadena = pMap_Node->first_attribute("height")->value();
		rows_number = atoi(cadena.c_str()); 
		
		//Calculamos el tama�o del mapa en funci�n de estos valores
		width = colums_number * cellWidth;
		height = rows_number * cellHeight;

		// Recorremos todas las celdas de la capa 
		// Las vamos agrupando por filas con un identificador correspondiente para que luego sea m�s f�cil y �ptimo el c�lculo de colisiones. 
		// Vamos creando las tools correspondientes y meti�ndolas en el grupo correspondiente dentro de ToolsLayer
		// Guardamos tambi�n una referencia de cada una de ellas en la celda correspondiente
		// ( En las tolls se incluyen las de tipo "wall" que delimitan el tablero )

		// El id para la primera fila
		size_t rowID = (size_t) 0;
				
		// Creamos la primera fila de celdas
		Shared_Ptr<BoardLayer::Row> newRow = Shared_Ptr<BoardLayer::Row>( new BoardLayer::Row( rowID ) );
		// Incluimos la fila en la capa
		this->cellsLayer->push_back( newRow );
	
		size_t i=0; // filas
		size_t j=0; // columnas

		// El id de la celda ( su columna )
		size_t cellID_asSize_t;
		// El id de la tool ( aun no lo usamos )
		String toolsID;

		// Cacheamos el nodo de la capa para poder recorrer sus tiles
		pLayer_Node=pMap_Node->first_node("layer");

		// Recorremos todos los nodos "Tiles" de la capa y los vamos agrupando por filas
		for( pTile_Node=pLayer_Node->first_node("tile"); pTile_Node; pTile_Node=pTile_Node->next_sibling() )
		{
			pos_x = j * cellWidth;
			pos_y = i * cellHeight;

			// Creamos una nueva celda con los datos que tenemos y la metemos en la fila actual
			// Creamos el identificador para la celda ( ser� su columna )
			cellID_asSize_t = (size_t)j;
			Shared_Ptr<Cell>newCell( new Cell( cellID_asSize_t, cellWidth, cellHeight, pos_x, pos_y ) );
			//La a�ado al grafo de escena
			newRow->push_back( newCell );

			// Obtenemos el id del Tile
			pAttr = pTile_Node->first_attribute("gid");
			cadena = pAttr->value();

			// Los tiles que tienen identificador "0" est�n vac�os, no tienen tools en un inicio
			if( cadena == "1" )
			{
				toolsID = "START";
				Shared_Ptr<Start>newObstacle( Shared_Ptr<Start>( new Start( toolsID, Tools::Start, Start::StartStates::UP ,pos_x, pos_y ) ) );
				//M�temos el obstaculo en la capa de las tools
				this->boardTools->push_back( newObstacle );
				//Guardamos la referencia en la celda
				newCell->tool = newObstacle.get();
				newCell->cellState = Cell::Occupied;

				//Configuramos el player para que salga en esta posici�n
				SingletonManagers::getInstance().game1Gameplay->player->setPlayer( pos_x, pos_y, cellWidth );

			}
			if( cadena == "2" )
			{
				toolsID = "OBSTACLE";
				Shared_Ptr<Obstacle>newObstacle( Shared_Ptr<Obstacle>( new Obstacle( toolsID, Tools::Obstacle, pos_x, pos_y ) ) );
				//M�temos el obstaculo en la capa de las tools
				this->boardTools->push_back( newObstacle );
				//Guardamos la referencia en la celda
				newCell->tool = newObstacle.get();
				newCell->cellState = Cell::Occupied;

			}
			else if( cadena == "3" )
			{
				toolsID = "WALL";
				Shared_Ptr<Wall>newWall( Shared_Ptr<Wall>( new Wall( toolsID, Tools::Wall, pos_x, pos_y ) ) );
				//M�temos el muro en la capa de las tools
				this->boardTools->push_back( newWall );
				//Guardamos la referencia en la celda
				newCell->tool = newWall.get();
				newCell->cellState = Cell::Occupied;

			}
			else if( cadena == "4" )
			{
				toolsID = "END";
				Shared_Ptr<Tools>newWall( Shared_Ptr<Tools>( new Tools( toolsID, Tools::End, 
																	SingletonManagers::getInstance().game1Gameplay->tools_sheet_1, 
																	"salidaleft",
																	pos_x, pos_y ) ) );
				//M�temos el muro en la capa de las tools
				this->boardTools->push_back( newWall );
				//Guardamos la referencia en la celda
				newCell->tool = newWall.get();
				newCell->cellState = Cell::Occupied;

			}
			
			// Si llegamos al final de la columna cambiamos de fila
			j ++;
			if( j >= colums_number )
			{
				j=0;
				i++;
				// El id para la nueva fila
				rowID = (size_t) i;
				// Creamos la nueva fila
				newRow = Shared_Ptr<BoardLayer::Row>( new BoardLayer::Row( rowID ) );
				// Incluimos la fila en la capa
				this->cellsLayer->push_back( newRow );
			}
		}
		
	}
}