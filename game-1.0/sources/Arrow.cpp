
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */


#include "Arrow.hpp"

#include "Player.hpp"

namespace game1
{

	//Constructor privado
	void Arrow::setArrow( ARROWDIRECTIONS _arrowDirection )
	{
		switch ( arrowDirection )
		{
		case LEFT:
			slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("arrowl");
			image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
				
		break;

		case UP:
			slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("arrowu");
			image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;

		break;

		case RIGHT:
			slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("arrowr");
			image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
				
		break;

		case DOWN:
			slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("arrowd");
			image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
				
		break;
		}

		//Asignamos el tama�o del slice en funci�n del porcentaje de escalado de los assets
		float size = slice->width;
		width = slice->width * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		height = slice->height * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		//Cacheamos el valor de la mitad del tama�o para luego poder calcular din�micamente el centro de la tool
		halfSize = Vector2f( width/2, height/2 );
	}

	//Act�a sobre el player para modificar sus propiedades
	void Arrow::changePlayerPropierties()
	{
		switch ( arrowDirection )
		{
		case LEFT:
			SingletonManagers::getInstance().game1Gameplay->player->changeDirecction( Player::PlayerDirection::LEFT );
				
		break;

		case UP:
			SingletonManagers::getInstance().game1Gameplay->player->changeDirecction( Player::PlayerDirection::UP );

		break;

		case RIGHT:
			SingletonManagers::getInstance().game1Gameplay->player->changeDirecction( Player::PlayerDirection::RIGHT );
				
		break;

		case DOWN:
			SingletonManagers::getInstance().game1Gameplay->player->changeDirecction( Player::PlayerDirection::DOWN );
				
		break;
		}
	}

}