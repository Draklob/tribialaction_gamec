
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Player.hpp"

#include "SingletonManagers.hpp"

#include "Tools.hpp"

//Para debuguear
#include <iostream>

namespace game1
{
	using namespace tribalAction;
	using namespace std;

	//M�todo que configura el player cuando le damos a ejecutar
	void Player::setPlayer( const float _posX, const float _posY, const float _cellWidth )
	{
		is_slice = true;
		//Establecemos la imagen para la textura
		image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
		slice = image->slice("playerup");
		//Creamos los slices para las distintas direcciones
		//Hay que sobreescribir el m�todo de draw
		leftSlice = image->slice("playerleft");
		upSlice = image->slice("playerup");
		rightSlice = image->slice("playerright");
		downSlice = image->slice("playerdown");

		//El slice para cuando est� muerto
		deadSlice = image->slice("playerdead");

		//Asignamos el tama�o del slice en funci�n del porcentaje del escalado de los assets
		float size = slice->width;
		width = slice->width * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		height = slice->height * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		//Cacheamos el valor de la mitad del tama�o para luego poder calcular din�micamente el centro de la tool
		halfSize = Vector2f( width/2, height/2 );

		//Establcemos su posici�n inicial
		initialPosition = Vector2f( _posX, _posY );
		this->set_node_location( initialPosition );

		//Distancia que tiene que recorrer para ir a la siguiente celda
		//En principio las celdas son cuadradas as� que el desplazamiento en cada direcci�n es el mismo
		cellsDistance = _cellWidth;
	}

	//M�todo que cambia la direcci�n y la textura de la casilla
	void Player::changeDirecction( PlayerDirection newPlayerDirection )
	{
		switch ( newPlayerDirection )
		{
		case LEFT:
			playerDirection = LEFT;
			slice = leftSlice;
				
		break;

		case UP:
			playerDirection = UP;
			slice = upSlice;

		break;

		case RIGHT:
			playerDirection = RIGHT;
			slice = rightSlice;
				
		break;

		case DOWN:
			playerDirection = DOWN;
			slice = downSlice;
				
		break;
		}
	}

	//Actualiza la posici�n del player cuando el juego est� en modo EXECUTION
	void Player::update	(float real_frame_delta)
	{
		if( playerState == Moving )
		{
			//Calculamos el tiempo transcurrido
			elapsedTransitionTime += real_frame_delta;
			float transitionTimePercantage = elapsedTransitionTime / totalTransitionTime;
			if( transitionTimePercantage >= 1 )
			{
				//Movemos el player para ajustarlo a la posici�n exacta de destino
				this->set_location( calculatePlayerPosition( 1 ) );
				//Si el player ha recorrido el trayecto
				//evaluamos las siguientes posibilidades
				playerState = Evaluating;
				evaluatePossibilitiesTree();
			}
			else
			{
				//Movemos el player en funci�n del tiempo transcurrido
				this->set_location( calculatePlayerPosition( transitionTimePercantage ) );
			}
		}

	}

	// Devuelve la posici�n del player pasado un tiempo determinado
	Vector2f Player::calculatePlayerPosition( const float elpasedTime )
	{
		//Calculamos la distancia recorrida del trayecto en funci�n del tiempo pasado
		float distance = cellsDistance * elpasedTime;
		Vector2f newPosition;

		switch ( playerDirection )
		{
		case LEFT:
			newPosition = Vector2f( initialPosition.x() - distance, initialPosition.y() );

		break;

		case UP:
			newPosition = Vector2f( initialPosition.x(), initialPosition.y() - distance );

		break;

		case RIGHT:
			newPosition = Vector2f( initialPosition.x() + distance, initialPosition.y() );
				
		break;

		case DOWN:
			newPosition = Vector2f( initialPosition.x(), initialPosition.y() + distance );
				
		break;
		}

		return newPosition;
	}

	// Eval�a las posibilidades para el player en funci�n de la celda en la que se encuentre
	// y la direcci�n que lleve actualmente
	void Player::evaluatePossibilitiesTree()
	{
		//Pongo primero todo esto para que el c�lculo de la siguiente posici�n se realice con initialPosition reseteado
		//En caso de que no se haya llegado a GameOver o a la meta el player sigue movi�ndose
		//Reseteamos las propiedades para la transici�n del player
		resetTransitionProperties();

		Vector2f playerPosition = this->get_location();
		//Lo primero es seleccionar la celda sobre la que est� el player
		Tools * tool = SingletonManagers::getInstance().game1Gameplay->level->board->getToolTypeOverCell( this->get_location() );
		//Si la celda est� vac�a devolver� null
		if( tool != null )
		{
			//Lo segundo es evaluar si ha llegado a la meta
			if( tool->toolType == Tools::ToolType::End )
			{
				cout << "ha llegado a la meta" << endl;
				playerState = Inactive;
				SingletonManagers::getInstance().game1Gameplay->endAchieve();
				return;
			}
			//Lo tercero ejecutar el m�todo de esta celda - void changePlayerPropierties();
			tool->changePlayerPropierties();
		}

		//Lo cuarto evaluar si la siguiente transici�n conduce a muerte por choque contra obst�culo o muro
		Vector2f nextPosition = calculatePlayerPosition( totalTransitionTime );
		tool = SingletonManagers::getInstance().game1Gameplay->level->board->getToolTypeOverCell( nextPosition );
		if( tool != null )
		{
			if( tool->toolType == Tools::ToolType::Obstacle || tool->toolType == Tools::ToolType::Wall )
			{
				cout << "ha perdido" << endl;
				playerState = Inactive;
				SingletonManagers::getInstance().game1Gameplay->gameOver();
				return;
			}
		}	
	}

	// M�todo que reseta las propiedades para la transici�n del player
	void Player::resetTransitionProperties()
	{
		elapsedTransitionTime = 0.f;
		playerState = Moving;
		initialPosition = get_location();
	}





}