
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						20 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "GamePlayUI.hpp"

#include "SingletonManagers.hpp"
#include "Sprite_Node.hpp"

#include "SimpleButton_Node.hpp"

#include <String.hpp>

//Para debuguear
#include <iostream>

#include "Utils.hpp"


namespace game1
{
	using namespace tribalAction;
	using namespace utils_1;
	using namespace toolkit;
	using namespace std;

	void GamePlayUI::initialize()
	{
		Shared_Ptr<Sprite_Node> bg = Shared_Ptr<Sprite_Node>( new Sprite_Node( "UI_bg", SingletonManagers::getInstance().game1Manager->ui_sheet_1, "hud" ) );
		//Le establecemos el tama�o
		bg->set_size( width, height );
		//La ajustamos en su posici�n
		coordY = SingletonManagers::getInstance().app_main_manager->screenSizef.y() - height;
		bg->set_location_y( coordY );
		//A�adimos el fondo al grupo ( grafo ) 
		this->push_back( bg );

		//Inicializamos el grupo de botones
		buttonsGroup = Shared_Ptr<ButtonsGroup>( new ButtonsGroup() );
		//Lo a�adimos a la escena
		this->push_back( buttonsGroup );

		// Calculamos la posici�n inicial para colocar el primer bot�n
		// ( la posici�n en la resoluci�n de dise�o ( 720 * 1280 ) es 328 * 1150 )
		Vector2f menuButtonPosition = Vector2f( 29 * SingletonManagers::getInstance().app_main_manager->designScaleFactor.x(), 
										 1115 * SingletonManagers::getInstance().app_main_manager->designScaleFactor.y() );

		// Calculamos la distancia entre cada bot�n dentro del hud
		// Relativa a la resoluci�n de dise�o ( 720 * 1280 )
		float buttonDistance = 22.f * SingletonManagers::getInstance().app_main_manager->designScaleFactor.x();
		//El ancho de cada bot�n
		float width;

		//Creo el sprite para el bot�n de menu
		String cadena = "menuButton";
		Shared_Ptr<SimpleButton_Node>menuButton( new SimpleButton_Node( "menuButton", SingletonManagers::getInstance().game1Manager->ui_sheet_1, 
													"menu", "menu" ) );
		//Cacheo su anchura para colocar el siguiente bot�n
		width = menuButton->width;
		//Lo situo
		menuButton->set_location(  Vector2f( menuButtonPosition.x(), menuButtonPosition.y() ) );
		//Lo agrego a la escena
		buttonsGroup->push_back( menuButton );

		//Creo el sprite para el bot�n de play
		cadena = "playButton";
		Shared_Ptr<SimpleButton_Node>playButton( new SimpleButton_Node( "playButton", SingletonManagers::getInstance().game1Manager->ui_sheet_1, 
													"play", "play" ) );
		//Cacheo su anchura para colocar el siguiente bot�n
		width = playButton->width;
		//Lo situo
		playButton->set_location(  Vector2f( menuButtonPosition.x() + buttonDistance + width, menuButtonPosition.y() ) );
		//Lo agrego a la escena
		buttonsGroup->push_back( playButton );

		//Creo el sprite para el bot�n de zoom
		cadena = "zoomButton";
		Shared_Ptr<SimpleButton_Node>zoomButton( new SimpleButton_Node( "zoomButton", SingletonManagers::getInstance().game1Manager->ui_sheet_1, 
													"lupa", "lupa" ) );
		//Cacheo su anchura para calcular el extremo del hud de botones
		//y optimizar el c�lculos de eventos touch
		width = zoomButton->width;
		float coordX = menuButtonPosition.x() + ( 2 * buttonDistance ) + ( 2 * width );
		rightBorder = coordX + width + menuButtonPosition.x();
		//Lo situo
		zoomButton->set_location(  Vector2f( coordX, menuButtonPosition.y() ) );
		//Lo agrego a la escena
		buttonsGroup->push_back( zoomButton );

	}

	//Le pasamos el evento pressed cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
	void GamePlayUI::pointer_pressed (const Vector2f & pointerPosition )
	{
		//Hemos hecho click en la zona de los botones
		cout << "buttons" << endl;
		//Recorremos el grupo de botones para comprobar si hemos hecho click en alguno de ellos
		ButtonsGroup::Iterator it;
		for (it = buttonsGroup->begin(); it != buttonsGroup->end(); ++it)
		{
			SimpleButton_Node *ptr = (dynamic_cast<SimpleButton_Node*>(it->get()));
			if (point_is_within_rectangle(pointerPosition.x(), pointerPosition.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
			{
				ptr->touched_inside = true;
				break;
			}
		}
	}

	//Le pasamos el evento released cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
	void GamePlayUI::pointer_released (const Vector2f & pointerPosition )
	{
		//Recorremos el grupo de botones para comprobar si hemos levantado el dedo sobre alguno de ellos
		ButtonsGroup::Iterator it;
		for (it = buttonsGroup->begin(); it != buttonsGroup->end(); ++it)
		{
			SimpleButton_Node *ptr = (dynamic_cast<SimpleButton_Node*>(it->get()));
			if (ptr->touched_inside == true)
			{
				ptr->touched_inside = false;

				if (point_is_within_rectangle(pointerPosition.x(), pointerPosition.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
				{
					if (ptr->get_id() == "menuButton")
					{
						cout << "menuButton" << endl;
					}
					else if (ptr->get_id() == "playButton")
					{
						cout << "playButton" << endl;
						SingletonManagers::getInstance().game1Gameplay->execute();
					}
					else if (ptr->get_id() == "zoomButton")
					{
						cout << "zoomButton" << endl;
					}
				}
			}
		}
	}
	
}

