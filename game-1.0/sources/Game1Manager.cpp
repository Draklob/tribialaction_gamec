
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Game1Manager.hpp"

#include "SingletonManagers.hpp"

//Incluimos las cabeceras de las vistas que controlamos desde el gestor principal
#include "Game1Menu.hpp"
#include "Game1Gameplay.hpp"
#include "Game1Help.hpp"

//Para debuguear
#include <iostream>

namespace game1
{
	using namespace snack;
	using namespace tribalAction;
	using namespace std;

    void Game1Manager::initialize ()
    {
		//Guardamos una referencia del manager principal del juego Game1 en la clase singleton
		SingletonManagers::getInstance().game1Manager = this;

		//La textura 1 con im�genes para la interfaz de usuario ( hud )
		ui_sheet_1 = Image::instance_of( SingletonManagers::getInstance().app_main_manager->path + "/game1/UI_sheet_1.png");

		//La textura 2 con im�genes para la interfaz de usuario ( menu )
		menu_sheet = Image::instance_of( SingletonManagers::getInstance().app_main_manager->path + "/game1/menu/menu_sheet.png");

		//De primeras lanzamos el menu del juego
		changeState( Game1States::MENU );

		// De momento siempre va empezar en el nivel 1 el player
		levelNum = 1;

    }

	//M�todo que cambia el estado de la aplicaci�n invocando el m�todo que inicia ese nuevo estado
	void Game1Manager::changeState( const Game1States newState )
	{
		//Actualizamos el estado principal de la app
		game1State = newState;

		switch (newState)
		{
			case MENU:
				start_Menu();
			break;

			case GAMEPLAY:
				start_Gameplay();
			break;
		
			case TUTORIAL:
				start_Help();
			break;
			
			case CREDITS:
			break;
			
			case EXIT:
			break;
			
				default:
				break;
		}
	}

	//Lanza el Menu
	void Game1Manager::start_Menu()
	{
		//Creamos un objeto de la clase intro
		game1Menu.reset( new Game1Menu );
		game1Menu->start();
	}

	void Game1Manager::start_Gameplay()
	{
		//Creamos un objeto de la clase intro
		game1Gameplay.reset(new Game1Gameplay);
		game1Gameplay->start();
	}

	//Lanza el juego Game1
	void Game1Manager::start_Help()
	{
		//Creamos un objeto de la clase GameManager
		game1Help.reset( new Game1Help );
		game1Help->start();
	}

	//Se invoca cuando se interrumpe la aplicaci�n bruscamente
	void Game1Manager::halt()
	{
		cout << "halt Game1" << endl;
		ui_sheet_1.reset();
		SingletonManagers::getInstance().game1Manager = null;
	}

}