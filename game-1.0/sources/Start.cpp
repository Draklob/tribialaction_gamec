
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						19 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Start.hpp"

namespace game1
{
	//Constructor privado
	void Start::setStart()
	{
		//Establecemos la imagen para la textura
		image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
		slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("entradaup");
		//Creamos los slices para las distintas direcciones
		//Hay que sobreescribir el m�todo de draw
		leftSlice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("entradaleft");
		upSlice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("entradaup");
		rightSlice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("entradaright");
		downSlice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("entradaup");

		//Asignamos el tama�o del slice en funci�n del porcentaje de escalado de los assets
		float size = slice->width;
		width = slice->width * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		height = slice->height * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		//Cacheamos el valor de la mitad del tama�o para luego poder calcular din�micamente el centro de la tool
		halfSize = Vector2f( width/2, height/2 );
	}

	//M�todo que cambia la direcci�n y la textura de la casilla
	void Start::changeDirecction()
	{
		switch ( startState )
		{
		case LEFT:
			startState = UP;
			playerDirectionChange = Vector2f( 0 , -1 );
			slice = upSlice;
				
		break;

		case UP:
			startState = RIGHT;
			playerDirectionChange = Vector2f( 0 , 1 );
			slice = rightSlice;

		break;

		case RIGHT:
			startState = DOWN;
			playerDirectionChange = Vector2f( 0 , 1 );
			slice = downSlice;
				
		break;

		case DOWN:
			startState = LEFT;
			playerDirectionChange = Vector2f( -1 , 0 );
			slice = leftSlice;
				
		break;
		}
	}


	//Act�a sobre el player para modificar sus propiedades
	void Start::changePlayerPropierties()
	{
	}
}