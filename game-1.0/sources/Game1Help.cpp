
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Game1Help.hpp"

#include "SingletonManagers.hpp"

#include <Transformable.hpp> 
#include <Renderer.hpp>
#include <Color.hpp>

//Para debuguear
#include <iostream>

#include "Utils.hpp"

#include "Sprite_Node.hpp"

namespace game1
{
	using namespace snack;
	using namespace tribalAction;
	using namespace std;
	using namespace utils_1;

	void Game1Help::initialize()
	{
		cout << "ha entrado en tutorial" << endl;

		//Creo la escena
		tutorialScene = Shared_Ptr<Scene>(new Scene());

		//Creo el grupo para los botones
		tutorialBtns_group = Shared_Ptr<MenuBtns_Group>(new MenuBtns_Group());
		//Lo agrego a la escena
		tutorialScene->push_back(tutorialBtns_group);

		//Voy creando cada bot�n 

		Shared_Ptr< Button_Node > button1(new Button_Node("BACK", SingletonManagers::getInstance().game1Manager->menu_sheet, "back", "back"));
		//Lo a�adimos al grupo ( grafo de escena )
		tutorialBtns_group->push_back(button1);

		Shared_Ptr< Button_Node > button2(new Button_Node("PREVIOUS", SingletonManagers::getInstance().game1Manager->menu_sheet, "previous", "previous"));
		//Lo a�adimos al grupo ( grafo de escena )
		tutorialBtns_group->push_back(button2);

		Shared_Ptr< Button_Node > button3(new Button_Node("NEXT", SingletonManagers::getInstance().game1Manager->menu_sheet, "next", "next"));
		//Lo a�adimos al grupo ( grafo de escena )
		tutorialBtns_group->push_back(button3);

		//Asignamos las propiedades al bot�n 1
		button1->set_location( SingletonManagers::getInstance().app_main_manager->screenSizef.x() * 0.2 - button1->width,
								SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.97 - button1->height);

		//Asignamos las propiedades al bot�n 2
		button2->set_location( (SingletonManagers::getInstance().app_main_manager->screenSizef.x() / 2) * 0.88 - button2->width, 
								SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.96 - button2->height);

		//Asignamos las propiedades al bot�n 3
		button3->set_location( (SingletonManagers::getInstance().app_main_manager->screenSizef.x() / 2) * 1.5 - button3->width, 
								SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.96 - button3->height);

		// Configuramos la 1� Imagen del Tutorial
		image1 = Shared_Ptr<Sprite_Node>(new Sprite_Node("IMAGE1", Image::instance_of(SingletonManagers::getInstance().app_main_manager->path + "/game1/tutorial/image1.jpg")));
		image1->set_size(SingletonManagers::getInstance().app_main_manager->screenSizef.x() * 0.8, SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.8);
		image1->set_location((SingletonManagers::getInstance().app_main_manager->screenSizef.x() / 2) - (image1->width / 2),
			(SingletonManagers::getInstance().app_main_manager->screenSizef.y() / 2.3) - (image1->height / 2));
		image1->visible = false;
		tutorialScene->push_back(image1);

		// Configuramos la 2� imagen del Tutorial
		image2 = Shared_Ptr<Sprite_Node>(new Sprite_Node("IMAGE2", Image::instance_of(SingletonManagers::getInstance().app_main_manager->path + "/game1/tutorial/image2.jpg")));
		image2->set_size(SingletonManagers::getInstance().app_main_manager->screenSizef.x() * 0.8, SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.8);
		image2->set_location((SingletonManagers::getInstance().app_main_manager->screenSizef.x() / 2) - (image2->width / 2),
			(SingletonManagers::getInstance().app_main_manager->screenSizef.y() / 2.3) - (image2->height / 2));
		image2->visible = false;
		tutorialScene->push_back(image2);

		// Configuramos la 3� imagen del Tutorial
		image3 = Shared_Ptr<Sprite_Node>(new Sprite_Node("IMAGE3", Image::instance_of(SingletonManagers::getInstance().app_main_manager->path + "/game1/tutorial/image3.jpg")));
		image3->set_size(SingletonManagers::getInstance().app_main_manager->screenSizef.x() * 0.8, SingletonManagers::getInstance().app_main_manager->screenSizef.y() * 0.8);
		image3->set_location((SingletonManagers::getInstance().app_main_manager->screenSizef.x() / 2) - (image3->width / 2),
			(SingletonManagers::getInstance().app_main_manager->screenSizef.y() / 2.3) - (image3->height / 2));
		image3->visible = false;
		tutorialScene->push_back(image3);

		imgTutorial = 0;

		is_visible = true;
	}


	//M�todo que se invoca en cada frame
	void Game1Help::render(Rasterizer & rasterizer)
	{
		if( is_visible )
		{
			rasterizer.clear(white);

			//Renderizamos los diferentes botones del men�
			Renderer::transform(*tutorialScene);
			Renderer::draw(*tutorialScene, rasterizer);

			if (imgTutorial == 0)
				image1->visible = true;
			else
				image1->visible = false;

			if (imgTutorial == 1)
				image2->visible = true;
			else
				image2->visible = false;

			if (imgTutorial == 2)
				image3->visible = true;
			else
				image3->visible = false;
		}

	}

	//M�todo que se invoca cuando tocamos con el dedo
	void Game1Help::pointer_pressed(const Pointer_Event & event)
	{
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_pressed(event);

		MenuBtns_Group::Iterator it;
		for (it = tutorialBtns_group->begin(); it != tutorialBtns_group->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (point_is_within_rectangle(pointerLocation.x(), pointerLocation.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
			{
				ptr->touched_inside = true;
				break;
			}
		}
	}

	void Game1Help::pointer_released(const Pointer_Event & event)
	{
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_released(event);

		MenuBtns_Group::Iterator it;
		for (it = tutorialBtns_group->begin(); it != tutorialBtns_group->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (ptr->touched_inside == true)
			{
				ptr->touched_inside = false;

				if (point_is_within_rectangle(pointerLocation.x(), pointerLocation.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
				{
					if (ptr->get_id() == "BACK")
					{
						is_visible = false;
						halt();
						this->stop();
						//SingletonManagers::getInstance().game1Manager->changeState(SingletonManagers::getInstance().game1Manager->MENU);
					}
					else if (ptr->get_id() == "PREVIOUS")
					{
						if (imgTutorial > 0)
							imgTutorial--;
					}
					else if (ptr->get_id() == "NEXT")
					{
						if (imgTutorial < 2)
							imgTutorial++;
					}
				}
				break;
			}
		}
	}


	//Se invoca cuando se interrumpe la aplicaci�n bruscamente
	void Game1Help::halt()
	{
		cout << "haltGame1Menu" << endl;

		image1.reset();
		image2.reset();
		image3.reset();

		tutorialBtns_group.reset();
		tutorialScene.reset();
	}

}