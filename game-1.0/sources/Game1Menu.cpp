
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Game1Menu.hpp"

#include "SingletonManagers.hpp"

#include <Transformable.hpp> 
#include <Renderer.hpp>
#include <Color.hpp>

//Para debuguear
#include <iostream>

#include "Utils.hpp"

#include "Sprite_Node.hpp"

namespace game1
{
	using namespace snack;
	using namespace tribalAction;
	using namespace std;
	using namespace utils_1;

    void Game1Menu::initialize ()
    {
		cout << "ha entrado en menu" << endl;

		//Creo la escena
		menuScene = Shared_Ptr<Scene>( new Scene() );

		//Creo el sprite para la imagen de fondo
		String id = "Game1Menu_bg";
		Shared_Ptr<Sprite_Node> bg = Shared_Ptr<Sprite_Node>( new Sprite_Node( id, Image::instance_of( SingletonManagers::getInstance().app_main_manager->path + "/game1/menu/bg.jpg" ) ) );
		bg->set_size( SingletonManagers::getInstance().app_main_manager->screenSizef.x(), SingletonManagers::getInstance().app_main_manager->screenSizef.y() );
		//Lo a�adimos al grafo de escena
		menuScene->push_back( bg );

		//Creo el grupo para los botones
		menuBtns_group = Shared_Ptr<MenuBtns_Group>( new MenuBtns_Group() );
		//Lo agrego a la escena
		menuScene->push_back( menuBtns_group );
		
		//Voy creando cada bot�n y calculo sus dimensiones en funci�n del texto y el padding
		float padding = 0.04f * SingletonManagers::getInstance().app_main_manager->screenSizef.x();

		Shared_Ptr< Button_Node > button1 ( new Button_Node( "START", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick" ) );
		button1->set_text( "Start", white, black, SingletonManagers::getInstance().app_main_manager->font_small ); 
		button1->calculate_button_size( padding, padding );
		//Lo a�adimos al grupo ( grafo de escena )
		menuBtns_group->push_back( button1 );

		Shared_Ptr< Button_Node > button2 ( new Button_Node( "TUTORIAL", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick" ) );
		button2->set_text( "Tutorial", white, black, SingletonManagers::getInstance().app_main_manager->font_small ); 
		button2->calculate_button_size( padding, padding );
		//Lo a�adimos al grupo ( grafo de escena )
		menuBtns_group->push_back( button2 );

		Shared_Ptr< Button_Node > button3 ( new Button_Node( "CREDITS", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick" ) );
		button3->set_text( "Credits", white, black, SingletonManagers::getInstance().app_main_manager->font_small ); 
		button3->calculate_button_size( padding, padding );
		//Lo a�adimos al grupo ( grafo de escena )
		menuBtns_group->push_back( button3 );

		Shared_Ptr< Button_Node > button4 ( new Button_Node( "EXIT", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick" ) );
		button4->set_text( "Exit", white, black, SingletonManagers::getInstance().app_main_manager->font_small ); 
		button4->calculate_button_size( padding, padding );
		//Lo a�adimos al grupo ( grafo de escena )
		menuBtns_group->push_back( button4 );

		//Calculo las dimensiones del bot�n que tenga el texto m�s largo y luego le aplico este tama�o a los dem�s
		float btn_width;
		if( button1->width > button2->width && button1->width > button3->width && button1->width > button4->width ) 
			btn_width  = button1->width;
		else if ( button2->width > button3->width && button2->width > button4->width ) 
			btn_width  = button2->width;
		else if ( button3->width > button4->width )
			btn_width  = button3->width;
		else  btn_width  = button4->width;


		//La altura es la misma para todos los botones
		float btn_height = button1->height;

		//Le aplico este tama�o a todos los botones
		button1->set_size( btn_width, btn_height );
		button2->set_size( btn_width, btn_height );
		button3->set_size( btn_width, btn_height );
		button4->set_size( btn_width, btn_height );
		

		//Calculo el left para todos los botones:
		float left = ( SingletonManagers::getInstance().app_main_manager->screenSizef.x() - btn_width ) / 2;

		//Calculamos el espaciado entre cada bot�n 
		float spacing = ( SingletonManagers::getInstance().app_main_manager->screenSizef.y() - 4 * btn_height ) / 5;

		//Calculo el top para el bot�n primero
		float top = spacing;

		//Asignamos las propiedades al bot�n 1
		button1->set_location( left, spacing );
		button1->center_text();
		//Asignamos las propiedades al bot�n 2
		button2->set_location( left, 2 * spacing + btn_height );
		button2->center_text();
		//Asignamos las propiedades al bot�n 3
		button3->set_location( left, 3 * spacing + 2 * btn_height );
		button3->center_text();
		//Asignamos las propiedades al bot�n 4
		button4->set_location( left, 4 * spacing + 3 * btn_height );
		button4->center_text();

    }


	//M�todo que se invoca en cada frame
    void Game1Menu::render (Rasterizer & rasterizer)
    {
        rasterizer.clear(Color(240, 240, 255));

		//Renderizamos los diferentes botones del men�
		Renderer::transform  (*menuScene);
        Renderer::draw       (*menuScene, rasterizer);

    }

	//M�todo que se invoca cuando tocamos con el dedo
    void Game1Menu::pointer_pressed (const Pointer_Event & event)
    {
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_pressed( event );

		MenuBtns_Group::Iterator it;
		for (it = menuBtns_group->begin(); it != menuBtns_group->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (point_is_within_rectangle(pointerLocation.x(), pointerLocation.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
			{
				ptr->touched_inside = true;
				break;
			}
		}
    }

	void Game1Menu::pointer_released (const Pointer_Event & event)
    {
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_released( event );

		MenuBtns_Group::Iterator it;
		for (it = menuBtns_group->begin(); it != menuBtns_group->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (ptr->touched_inside == true)
			{
				ptr->touched_inside = false;

				if (point_is_within_rectangle(pointerLocation.x(), pointerLocation.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
				{
					if (ptr->get_id() == "START")
					{
						SingletonManagers::getInstance().game1Manager->changeState(SingletonManagers::getInstance().game1Manager->GAMEPLAY);
					}
					else if (ptr->get_id() == "TUTORIAL")
					{
						SingletonManagers::getInstance().game1Manager->changeState(SingletonManagers::getInstance().game1Manager->TUTORIAL);
					}
					else if (ptr->get_id() == "CREDITS")
					{
					}
					else if (ptr->get_id() == "EXIT")
					{
					}
				}
			}
		}
	}


	//Se invoca cuando se interrumpe la aplicaci�n bruscamente
	void Game1Menu::halt()
	{
		cout << "haltGame1Menu" << endl;
	
		bg_image.reset();

		//menuScene.reset();

		menuBtns_group.reset();
		menuScene.reset();
	}

}