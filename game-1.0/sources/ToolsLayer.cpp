
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "ToolsLayer.hpp"

#include "SingletonManagers.hpp"

#include "Tools.hpp"

#include "Utils.hpp"

//Para debuguear
#include <iostream>

#include "Cell.hpp"

namespace game1
{
	using namespace tribalAction;
	using namespace utils_1;
	using namespace std;

	ToolsLayer::ToolsLayer()
	{
		//Inicializamos el estado
		toolsState = Ready;

		// Calculamos la posici�n inicial para colocar la primera tool
		// ( la posici�n en la resoluci�n de dise�o ( 720 * 1280 ) es 328 * 1150 )
		toolsInitialPosition = Vector2f( 328 * SingletonManagers::getInstance().app_main_manager->designScaleFactor.x(), 
										 1100 * SingletonManagers::getInstance().app_main_manager->designScaleFactor.y() );

		// Calculamos la distancia entre cada tool dentro del hud
		// Relativa a la resoluci�n de dise�o ( 720 * 1280 )
		toolsDistance = 30 * SingletonManagers::getInstance().app_main_manager->designScaleFactor.x();
	}

	//Posiciona las tools de la capa hudTools
	void ToolsLayer::resetToolsPositions()
	{
		//El contador para ir incrementando la posici�n en horizontal de la tool en el hud
		size_t i = 0;
		Iterator it;
		for (it = this->begin(); it != this->end(); ++it)
		{
			//Casteamos din�micamente de Node a Tools
			Tools *ptr = (dynamic_cast<Tools*>(it->get()));
			//Asignamos la posici�n de la tool pero primero la guardamos como la posici�n inicial en el hud
			ptr->initialPosition = Vector2f( toolsInitialPosition.x() + i * ( toolsDistance + ptr->width ), toolsInitialPosition.y() );
			ptr->set_location( ptr->initialPosition );
			//Incrementamos el contador
			i++;
		}
	}

	//Le pasamos el evento pressed cuando hemos pulsado en la zona derecha del hud ( la de las tools )
	void ToolsLayer::pointer_pressed (const Vector2f & pointerPosition )
	{
		//Si no estamos arrastrando y aun quedan tools por posicionar
		if( toolsState == Ready )
		{
			//Recorremos el grupo de tools para comprobar si hemos hecho click en alguna de ellas
			Iterator it;
			for (it = this->begin(); it != this->end(); ++it)
			{
				Tools *ptr = (dynamic_cast<Tools*>(it->get()));
				if (point_is_within_rectangle(pointerPosition.x(), pointerPosition.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
				{
					ptr->touched_inside = true;
					//Guargamos la distancia entre la tool y la posici�n del click
					relativeDistance = Vector2f( ptr->get_location_x() - pointerPosition.x(), ptr->get_location_y() - pointerPosition.y() );
					break;
				}
			}
		}
	}

	//Le pasamos el evento dragged cuando estamos arrastrando en el estado de PLANNING
	void ToolsLayer::pointer_dragged (const Vector2f & pointerPosition )
	{
		//Recorremos el grupo de tools para comprobar si estamos arrastrando alguna de ellas
		Iterator it;
		for (it = this->begin(); it != this->end(); ++it)
		{
			Tools *ptr = (dynamic_cast<Tools*>(it->get()));
			if (ptr->touched_inside == true)
			{
				toolsState = Dragging;
				Vector2f newPosition = Vector2f( pointerPosition.x() + relativeDistance.x(), pointerPosition.y() + relativeDistance.y() );
				ptr->set_location( newPosition );
				//Comprobamos si la tool est� sobre una celda vac�a para iluminarla
				SingletonManagers::getInstance().game1Gameplay->level->board->showOnCell( ptr->getCenter( newPosition ) );
				break;
			}
		}
	}

	//Le pasamos el evento released cuando hemos pulsado en la zona derecha del hud ( la de las tools )
	void ToolsLayer::pointer_released (const Vector2f & pointerPosition )
	{
		//Si estamos arrastrando recorremos el grupo de tools 
		//para comprobar cual de ellas est�bamos arrastrando
		if( toolsState == Dragging )
		{
			toolsState = Ready;

			//Por si tenemos que eliminar una tool concreta
			//"pos" indica la posici�n de la tool dentro del grupo ( toolsLayer )
			size_t pos = 0;

			Iterator it;
			for (it = this->begin(); it != this->end(); ++it)
			{
				Tools* ptr = (dynamic_cast<Tools*>(it->get()));
				
				//string cadena = ptr->get_id();
				//cout << "id = " << cadena << endl;
				
				if (ptr->touched_inside == true)
				{
					//Comprobamos si hab�a una celda seleccionada en el tablero
					if( SingletonManagers::getInstance().game1Gameplay->level->board->highlightedCell != null )
					{
						//Le restamos uno a las tools que quedan por colocar
						toolsStillInHud--;
						//cout << "toolsStillInHud = " << toolsStillInHud << endl;
						if( toolsStillInHud <= 0 )
						{
							toolsState = AllLocated;
							cout << "allalocated" << endl;
						}
						//La sacamos de este grupo ( layer ) para meterla en la layer de las tools sobre el tablero ( boardTools )
						//this->erase( pos );

						/*//La metemos en la capa de las tools del tablero
						cout << "size : " << SingletonManagers::getInstance().game1Gameplay->level->board->boardTools->size() << endl;
						//Shared_Ptr<Tools>tool = Shared_Ptr<Tools>( ptr );
						//SingletonManagers::getInstance().game1Gameplay->level->board->boardTools->push_back( tool );
						//SingletonManagers::getInstance().game1Gameplay->level->board->instertIntoToolsLayer( ptr );
						cout << "size : " << SingletonManagers::getInstance().game1Gameplay->level->board->boardTools->size() << endl;*/

						//La centramos en esa posici�n
						Vector2f cellPosition = SingletonManagers::getInstance().game1Gameplay->level->board->gethighlightedCellPosition();
						ptr->setCenteredIntoCell( cellPosition );
						//Ponemos como ocupada la celda que estuviera resaltada en el tablero
						//y guardamos una referencia a la tool en esta celda
						SingletonManagers::getInstance().game1Gameplay->level->board->highlightedCell->tool = ptr;
						SingletonManagers::getInstance().game1Gameplay->level->board->highLightedToOccupied();
						//break;
					}
					else // la devolvemos a su posici�n inicial en el hud
					{
						ptr->resetInitialPosition();
					}
				}
				ptr->touched_inside = false;
				pos++;
			}
		}
	}

	
}








