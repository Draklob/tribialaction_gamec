
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						20 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "GameOver.hpp"

#include "SingletonManagers.hpp"
#include "Sprite_Node.hpp"

#include "Button_Node_2.hpp"

#include "Button_Node.hpp"

#include <String.hpp>

//Para debuguear
#include <iostream>

#include "Utils.hpp"


namespace game1
{
	using namespace tribalAction;
	using namespace utils_1;
	using namespace toolkit;
	using namespace std;

	void GameOver::initialize()
	{
		Shared_Ptr<Sprite_Node> bg = Shared_Ptr<Sprite_Node>( new Sprite_Node( "UI_bg", SingletonManagers::getInstance().game1Manager->ui_sheet_1, "gameover" ) );
		//Le establecemos el tama�o
		bg->set_size( width, height );
		//La ajustamos en su posici�n
		this->set_location( initialPosition );
		//A�adimos el fondo al grupo ( grafo ) 
		this->push_back( bg );

		//Inicializamos el grupo de botones
		buttonsGroup = Shared_Ptr<ButtonsGroup>( new ButtonsGroup() );
		//Lo a�adimos a la escena
		this->push_back( buttonsGroup );

		//Voy creando cada bot�n y calculo sus dimensiones en funci�n del texto y el padding
		float padding = 0.04f * SingletonManagers::getInstance().app_main_manager->screenSizef.x();

		Shared_Ptr< Button_Node_2 > button1 ( new Button_Node_2( "RESET", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick", 2.f, 2.f ) );
		//Lo a�adimos al grupo ( grafo de escena )
		buttonsGroup->push_back( button1 );

		Shared_Ptr< Button_Node_2 > button2 ( new Button_Node_2( "MENU", SingletonManagers::getInstance().game1Manager->menu_sheet, "button", "buttonclick", 2.f, 2.f ) );
		//Lo a�adimos al grupo ( grafo de escena )
		buttonsGroup->push_back( button2 );

		//Calculo las dimensiones del bot�n que tenga el texto m�s largo y luego le aplico este tama�o al otro
		float btn_width;
		if( button1->width > button2->width ) 
			btn_width  = button1->width;
		else btn_width  = button2->width;
	
		//La altura es la misma para los dos botones
		float btn_height = button1->height;

		//Le aplico este tama�o a los dos botones
	/*	button1->set_size( btn_width, btn_height );
		button2->set_size( btn_width, btn_height );
	
		//Calculo el left, el mismo para los dos botones
		float left = ( SingletonManagers::getInstance().app_main_manager->screenSizef.x() - btn_width ) / 2 - initialPosition.x();

		//Calculamos el espaciado entre cada bot�n 
		float spacing = ( SingletonManagers::getInstance().app_main_manager->screenSizef.y() - 4 * btn_height ) / 5;

		//Calculo el top para el bot�n primero
		float top = spacing;

		//Asignamos las propiedades al bot�n 1
		button1->set_location( left, spacing );
		button1->center_text();
		//Asignamos las propiedades al bot�n 2
		button2->set_location( left, 2 * spacing + btn_height );
		button2->center_text();	*/

	}

	//Le pasamos el evento pressed cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
	void GameOver::pointer_pressed (const Vector2f & pointerPosition )
	{
		//Hemos hecho click en la zona de los botones
		cout << "buttons" << endl;
		//Recorremos el grupo de botones para comprobar si hemos hecho click en alguno de ellos
		ButtonsGroup::Iterator it;
		for (it = buttonsGroup->begin(); it != buttonsGroup->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (point_is_within_rectangle(pointerPosition.x(), pointerPosition.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
			{
				ptr->touched_inside = true;
				break;
			}
		}
	}

	//Le pasamos el evento released cuando hemos pulsado en la zona izquierda del hud ( la de los botones )
	void GameOver::pointer_released (const Vector2f & pointerPosition )
	{
		//Recorremos el grupo de botones para comprobar si hemos levantado el dedo sobre alguno de ellos
		ButtonsGroup::Iterator it;
		for (it = buttonsGroup->begin(); it != buttonsGroup->end(); ++it)
		{
			Button_Node *ptr = (dynamic_cast<Button_Node*>(it->get()));
			if (ptr->touched_inside == true)
			{
				ptr->touched_inside = false;

				if (point_is_within_rectangle(pointerPosition.x(), pointerPosition.y(), ptr->get_location_x(), ptr->get_location_y(), ptr->width, ptr->height))
				{
					if (ptr->get_id() == "menuButton")
					{
						cout << "menuButton" << endl;
					}
					else if (ptr->get_id() == "playButton")
					{
						cout << "playButton" << endl;
						SingletonManagers::getInstance().game1Gameplay->execute();
					}
					else if (ptr->get_id() == "zoomButton")
					{
						cout << "zoomButton" << endl;
					}
				}
			}
		}
	}
	
}

