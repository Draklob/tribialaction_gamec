
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						03 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Game1Gameplay.hpp"
#include "SingletonManagers.hpp"

#include "LevelConstructor.hpp"
#include "Sprite_Node.hpp"

#include <Renderer.hpp>

#include "Player.hpp"

//Para debuguear
#include <iostream>

namespace game1
{
	using namespace tribalAction;
	using namespace utils_1;
	using namespace snack;
	using namespace std;

	void Game1Gameplay::initialize()
	{
		// Guardamos la referencia en el Singleton
		SingletonManagers::getInstance().game1Gameplay = this;

		// Cuando se inicia la partida siempre se empieza por el estado de LOADING
		gameMainState = GameMainStates::LOADING;

		// La textura 1 con im�genes para las herramientas del player
		tools_sheet_1 = Image::instance_of(SingletonManagers::getInstance().app_main_manager->path + "/game1/gameplay/tools_sheet_1.png");

		// Creamos la escena
		gamePlayScene = Shared_Ptr<Scene>( new Scene() );

		// Le agregamos la imagen de fondo
		String id = "Game1GamePlay_bg";
		Shared_Ptr<Sprite_Node> bg ( new Sprite_Node( id, Image::instance_of( SingletonManagers::getInstance().app_main_manager->path + "/game1/gameplay/bg.jpg" ) ) );
		bg->set_size( SingletonManagers::getInstance().app_main_manager->screenSizef.x(), SingletonManagers::getInstance().app_main_manager->screenSizef.y() );
		gamePlayScene->push_back( bg );

		// Creamos la c�mara
		playerCamera = Shared_Ptr<Player_Camera>( new Player_Camera() );
		// La a�adimos a la escena
		gamePlayScene->push_back( playerCamera );

		// Creamos la UI 
		gamePlayUI = Shared_Ptr<GamePlayUI>( new GamePlayUI( SingletonManagers::getInstance().app_main_manager->screenSizef.x(), 0.2f * SingletonManagers::getInstance().app_main_manager->screenSizef.y()) );
		// La inicializamos
		gamePlayUI->initialize();
		// La a�adimos a la escena
		gamePlayScene->push_back( gamePlayUI );

		// Creamos la capa para las tools  ( lo �ltimo para colocarla encima de todo lo dem�s )
		// En el LevelConstructor se ir�n cargando las diferentes tools e incluyendo en esta capa
		toolsLayer = Shared_Ptr<ToolsLayer>( new ToolsLayer() );
		// La a�adimos a la escena
		gamePlayScene->push_back( toolsLayer );

		//Creamos el player y lo a�adimos a la escena ( aunque lo configuramos en el constructor de cada nivel )
		player = Shared_Ptr<Player>( new Player() );

		//Creamos la pantalla de GameOver y la a�adimos a la escena
		//Calculamos su tama�o
		float gameOverWidth = SingletonManagers::getInstance().app_main_manager->screenSizef.x() * 0.6f;
		float auxHeight = SingletonManagers::getInstance().app_main_manager->screenSizef.y() - gamePlayUI->height;
		float gameOverHeight = gameOverWidth * 0.8f;
		float gameOverPosX = ( SingletonManagers::getInstance().app_main_manager->screenSizef.x() - gameOverWidth ) / 2;
		float gameOverPosY = ( auxHeight - gameOverHeight ) / 2;
		gameOverLayer = Shared_Ptr<GameOver>( new GameOver( Vector2f( gameOverPosX, gameOverPosY ), gameOverWidth, gameOverHeight ) );
		gameOverLayer->initialize();
		//gamePlayScene->push_back( gameOverLayer );
		
		// Llamamos a resetLevel para iniciar el nivel sea el mismo o el siguiente cuando superamos el nivel
		resetLevel();	
	}

	//M�todo que comienza el estado de ejecuci�n ( una vez pulsado "Play" )
	void Game1Gameplay::execute()
	{
		cout << "excute" << endl;
		//Tan solo pasamos al estado EXECUTE si antes est�bamos en PLANNING
		if( gameMainState == GameMainStates::PLANNING )
		{
			gameMainState = GameMainStates::EXECUTION;
			//Lo primero ponemos el player en la posici�n de salida
			player->activatePlayer();
		}
	}

	void Game1Gameplay::pointer_pressed (const Pointer_Event & event)
	{
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_pressed(event);
		
		//Si hacemos click sobre el hud
		if( pointerLocation.y() >= gamePlayUI->coordY )
		{
			// Vemos si hemos hecho click en la zona de la izquierda o en la zona de la derecha del hud
			if( pointerLocation.x() >= gamePlayUI->rightBorder )
			{
				//Solo le pasamos el evento si estamos en estado de planificaci�n
				if( gameMainState == PLANNING )
				toolsLayer->pointer_pressed( pointerLocation );
			}
			else
			{
				//Le pasamos el evento a la zona de botones del hud
				gamePlayUI->pointer_pressed( pointerLocation );
			}
		}
		else 
		{

		}
	}

	void Game1Gameplay::pointer_dragged (const Pointer_Event & event)
	{
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_dragged(event);

		//Si estamos en el estado de PLANNING le pasamos el evento de dragging al toolsLayer
		if( gameMainState == PLANNING )
		toolsLayer->pointer_dragged( pointerLocation );
	}

	void Game1Gameplay::pointer_released (const Pointer_Event & event)
	{
		//Invocamos primero el m�todo de la clase padre
		UI::pointer_released(event);

		//Como podemos soltar el dedo en cualquier lugar de la pantalla
		//le pasamos el evento a todas las capas
		toolsLayer->pointer_released( pointerLocation );
		gamePlayUI->pointer_released( pointerLocation );
		
	}

	void Game1Gameplay::update (float  real_frame_delta) 
	{
		switch (gameMainState)
		{
		case LOADING:
			break;
		case PLANNING:

			break;
		case EXECUTION:
			//Actualizamos la posici�n del player
			player->update( real_frame_delta );
			break;
		default:
			break;
		}
	}


	void Game1Gameplay::render(Rasterizer & rasterizer)
	{
		rasterizer.clear (white); 

		Renderer::transform( *gamePlayScene );
		Renderer::draw( *gamePlayScene, rasterizer );
	}

	void Game1Gameplay::resetLevel()
	{
		// Construimos el nivel y lo a�adimos al grafo de escena ( a la c�mara )
		level = constructLevel(SingletonManagers::getInstance().game1Manager->levelNum);
		// Lo a�adimos a la escena
		playerCamera->push_back( level->board );
		// A�adimos el player a la escena por encima del tablero
		playerCamera->push_back( player );
		// Cambiamos el estado a PLANNING
		gameMainState = GameMainStates::PLANNING;
	}

	void Game1Gameplay::halt()
	{

	}
}