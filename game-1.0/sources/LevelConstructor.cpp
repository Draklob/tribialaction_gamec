
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */


#include "LevelConstructor.hpp"

#include "Arrow.hpp"
#include "Tools.hpp"

#include <String.hpp>

namespace game1
{
	using namespace toolkit;

	//M�todo que construye el nivel pasado por par�metro
	Shared_Ptr<Level> constructLevel( size_t levelNumber )
	{
		//Inicializo el puntero para el nivel
		Shared_Ptr<Level> level = Shared_Ptr<Level>( new Level() );

		//Inicializo el punto para el tablero
		level->board = Shared_Ptr<Board>( new Board()); 

		//Variable para pasarles el id a los objetos
		String cadena;

		switch (levelNumber)
		{
			case 1: //Level 1

				//Se a�aden los objetos correspondientes al pull de objetos del player para este nivel
				//Y los vamos a�adiendo a la capa correspondiente
				cadena = "Tool1";
				SingletonManagers::getInstance().game1Gameplay->toolsLayer->push_back( Shared_Ptr<Arrow>( new Arrow( cadena, Tools::Arrow, Arrow::LEFT ) ) );
				cadena = "Tool2";
				SingletonManagers::getInstance().game1Gameplay->toolsLayer->push_back( Shared_Ptr<Arrow>( new Arrow( cadena, Tools::Arrow, Arrow::LEFT ) ) );
				cadena = "Tool3";
				SingletonManagers::getInstance().game1Gameplay->toolsLayer->push_back( Shared_Ptr<Arrow>( new Arrow( cadena, Tools::Arrow, Arrow::UP ) ) );

				//Asignamos el valor del n�mero de tools inicial que tenemos para situar en el mapa
				SingletonManagers::getInstance().game1Gameplay->toolsLayer->toolsStillInHud = 3;

				//Se carga el mapa correspondiente para este nivel
				level->board->loadBoard( "maps/level1/map.xml" );

				//En este nivel en el que el tablero es m�s peque�o lo centramos en pantalla
				level->board->centerBoard();
				
			break;

			case 2:
				
			break;
		

			
			default:
			break;
		}

		//Una vez cargadas las tools las situamos en el hud
		SingletonManagers::getInstance().game1Gameplay->toolsLayer->resetToolsPositions();

		return level;
	}
}
