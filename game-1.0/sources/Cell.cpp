
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#include "Cell.hpp"

#include "SingletonManagers.hpp"

namespace game1
{
	using namespace tribalAction;

	//M�todo que configura la celda
	Cell::Cell( const size_t _cellID, const float _width, const float _height, const float _posX, const float _posY )
		: Sprite_Node(), id_asSize_t( _cellID ), cellState( Free )
	{
		is_slice = true;
		image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
		slice = image->slice( "basecell" );
		normalSlice = image->slice( "basecell" );
		highLightedSlice = image->slice( "over" );
		this->set_size( _width, _height );
		this->set_node_location( _posX, _posY );

		//Inicializamos el puntero inteligente a Tools
		tool = null;
	}

	//M�todo que cambia el estado de la celda
	void Cell::changeCellState( CellStates newCellState )
	{
		cellState = newCellState;

		switch (newCellState)
		{
		case game1::Cell::Free:
			slice = normalSlice;
			break;
		case game1::Cell::HighLighted:
			slice = highLightedSlice;
			break;
		case game1::Cell::Occupied:
			slice = normalSlice;
			break;
		default:
			break;
		}
	}

}