
/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ *\
@																	   @
@						Javier Barreiro Portela						   @
@																	   @
@						07 de Julio del 2014						   @
@																	   @
@						jbarreiro.23@gmail.com						   @
@																	   @
@						@2014 JBPCode								   @
@																	   @
\* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @  @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */


#include "Obstacle.hpp"


namespace game1
{
	//Constructor privado
	void Obstacle::setObstacle()
	{
		slice = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1->slice("obstacle");
		image = SingletonManagers::getInstance().game1Gameplay->tools_sheet_1;
		//Asignamos el tama�o del slice en funci�n del porcentaje de escalado de los assets
		float size = slice->width;
		width = slice->width * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		height = slice->height * SingletonManagers::getInstance().app_main_manager->assetsScaleFactor;
		//Cacheamos el valor de la mitad del tama�o para luego poder calcular din�micamente el centro de la tool
		halfSize = Vector2f( width/2, height/2 );
	}

	//Act�a sobre el player para modificar sus propiedades
	void Obstacle::changePlayerPropierties()
	{
	}

}