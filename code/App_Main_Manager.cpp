/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel Ángel Torres                           *
*                                                                             *
*  Julio 2014  - Programación para móviles                                    *
*                                                                             *
*  Ingeniería y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "App_Main_Manager.hpp"
#include <Display.hpp>

#include "SingletonManagers.hpp"

//Incluimos las cabeceras de las vistas que controlamos desde el gestor principal
#include "Intro.hpp"
#include "Game1Manager.hpp"

#include <iostream>
#include <sstream>

namespace tribalAction
{
	using namespace toolkit;
	using namespace snack;

	using namespace std;

    void App_Main_Manager::initialize ()
    {
		//Guardamos una referencia del manager principal de la app en la clase singleton
		SingletonManagers::getInstance().app_main_manager = this;

		//Bloqueamos la orientación a portrait
		Display::lock_orientation( Display::PORTRAIT );

		//Para castear el tamaño de pantalla de int ( pixels ) a float
		screenSizef = Vector2f( (float)Display::width(), (float)Display::height() );
		
		//El factor de escala con respecto al diseño ( 720 * 1280 )
		//Para poder colocar los objetos en una posición absoluta en cualquier pantalla
		designScaleFactor = Vector2f( screenSizef.x()/720, screenSizef.y()/1280 );

		//Creamos las distintas fuentes en función del tamaño de la pantalla del dispositivo
		font_micro.reset (new Vector_Font( "/fonts/Puzzle.ttf", int( 25 * designScaleFactor.x() ) ));
		font_small.reset (new Vector_Font( "/fonts/Puzzle.ttf",  int( 30 * designScaleFactor.x() ) ));
		font_medium.reset (new Vector_Font( "/fonts/Puzzle.ttf",  int( 35 * designScaleFactor.x() ) ));
		font_big.reset (new Vector_Font( "/fonts/Puzzle.ttf",  int( 40 * designScaleFactor.x() ) ));

		//En función el tamaño de pantalla elegimos un path u otro para los assets
		//Y definimos el factor de escala para los assets
		if( screenSizef.x() >= 768 )//Diseño de referencia -> 1080 * 1920 ( Tiles 135 * 135 ) 
		{
			deviceType = HIGH;
			//Definimos el path donde buscar los recursos
			path = "/high/";
			//Definimos el factor de escala
			assetsScaleFactor = screenSizef.x() / 1080;
		}
		else if( screenSizef.x() >= 480 )//Diseño de referencia -> 720 * 1080 ( Tiles 90 * 90  )
		{
			deviceType = MEDIUM;
			//Definimos el path donde buscar los recursos
			path = "/mediu/";
			//Definimos el factor de escala
			assetsScaleFactor = screenSizef.x() / 720;
		}
		else if( screenSizef.x() < 480 )//Diseño de referencia -> 320 * 480 ( Tiles 40 * 40 ) 
		{
			deviceType = LOW;
			//Definimos el path donde buscar los recursos
			path = "/low/";
			//Definimos el factor de escala
			assetsScaleFactor = screenSizef.x() / 320;
		}
		
		//Inicializamos la variable que nos indica el idioma seleccionado
		//Hay que aprender a obtener el idioma configurado en el dispositivo
		//Y luego que se pueda cambiar y guardar dicho cambio
		idiom = SPANISH;
		//Cargamos los textos según el idioma
		load_texts();

		//Lanzamos la intro
		changeState( AppStates::INTRO );

    }

	//Método que cambia el estado de la aplicación invocando el método que inicia ese nuevo estado
	void App_Main_Manager::changeState( const AppStates newState )
	{
		//Actualizamos el estado principal de la app
		appState = newState;

		switch (newState)
		{
			case INTRO:
				start_Intro();
			break;
			case GAME1:
				start_Game1();
				break;
			default:
				break;
		}
	}

	//Lanza la Intro
	void App_Main_Manager::start_Intro()
	{
		//Creamos un objeto de la clase intro
		intro.reset( new Intro );
		intro->start();
	}

	//Lanza el juego Game1
	void App_Main_Manager::start_Game1()
	{
		//Creamos un objeto de la clase GameManager
		game1Manager.reset( new game1::Game1Manager );
		game1Manager->start();
	}

	//Se invoca cuando se interrumpe la aplicación bruscamente
	void App_Main_Manager::halt()
	{
		cout << "haltApp" << endl;
		SingletonManagers::getInstance().app_main_manager = null;
	}

	//Método que carga los textos según el idioma
	void App_Main_Manager::load_texts()
	{
		//Declaramos un objeto de la librería rapidxml de tipo xml_document
		xml_document<> doc;

		string path;

		if( idiom == SPANISH )
		path = "texts/spanish_texts.xml";
		else if( idiom == ENGLISH )
		path = "texts/english_texts.xml";

		std::ifstream file( path.c_str() );

		std::stringstream buffer;
		buffer << file.rdbuf();
		file.close();
		std::string content(buffer.str());

		doc.parse<0>(&content[0]);
		
		//El nodo raiz
		xml_node<> *pRoot = doc.first_node();
		xml_attribute<> *pAttr;
		typedef pair<string, string> text_translation_pair;
		std::string text_id;
		std::string text_translation;


		//Recorremos el árbol de nodos del archivo xml y los vamos volcando en el mapa
		for(xml_node<> *pNode=pRoot->first_node("text"); pNode; pNode=pNode->next_sibling())
		{
			pAttr = pNode->first_attribute("id");
			text_id = pAttr->value();
			char * source = pNode->value();
			convert_utf8_to_string(source, strlen(source), text_translation);

			
			cout<<text_id<<endl;
			cout<<text_translation<<endl;;
			//Insertamos el identificador de texto y su traducción en el mapa
			texts_map.insert( text_translation_pair( text_id, text_translation ));
		}
	}

		
	/* EXTERN FUNCTION */
	template< class OUTPUT_CONTAINER_TYPE >
	OUTPUT_CONTAINER_TYPE & App_Main_Manager::convert_utf8_to_string (const char * input_begin, size_t input_size, OUTPUT_CONTAINER_TYPE & output)
	{
			typedef typename OUTPUT_CONTAINER_TYPE::value_type char_type;
     
			output.resize  (0);
			output.reserve (input_size);
     
			for (const char * input_iterator = input_begin, * input_end = input_begin + input_size; input_iterator < input_end; )
			{
				unsigned  first_byte   = *input_iterator++;
				char_type unicode_char = 0;
     
				if (first_byte < 128)
				{
					unicode_char = char_type(first_byte);
				}
				else
				if ((first_byte & 0x20) == 0)
				{
					unsigned second_byte = *input_iterator++ & 0x7F;
     
					unicode_char = char_type(((first_byte & 0x3F) <<  6) | second_byte);
				}
				else
				if ((first_byte & 0x10) == 0)
				{
					unsigned second_byte = *input_iterator++ & 0x7F;
					unsigned  third_byte = *input_iterator++ & 0x7F;
     
					unicode_char = char_type(((first_byte & 0x1F) << 12) | (second_byte <<  6) | third_byte);
				}
				else
				if ((first_byte & 0x08) == 0)
				{
					unsigned second_byte = *input_iterator++ & 0x7F;
					unsigned  third_byte = *input_iterator++ & 0x7F;
					unsigned fourth_byte = *input_iterator++ & 0x7F;
     
					unicode_char = char_type
					(
						((first_byte & 0x0F) << 18) | (second_byte << 12) | (third_byte << 6) | fourth_byte
					);
				}
     
				output.push_back (unicode_char);
			}
     
			return (output);
		}
	/* / EXTERN */

}
