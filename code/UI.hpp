/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel �ngel Torres                           *
*                                                                             *
*  Julio 2014  - Programaci�n para m�viles                                    *
*                                                                             *
*  Ingenier�a y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef UI_HEADER
#define UI_HEADER

	#include <View.hpp>
	#include <Pointer_Event.hpp>
	#include <Rasterizer.hpp>

	namespace tribalAction
    {
		using snack::View;
		using snack::Pointer_Event;
		using snack::Rasterizer;

		using toolkit::Vector2f;
		
		class UI : public View
		{

		public:

			

		protected:

			//La posici�n del puntero
			Vector2f pointerLocation;

		protected:
		
			//M�todo que virtuales de View que se sobreescriben
			virtual void initialize         ();
            //virtual void pause              () { }
            //virtual void resume             () { }
            //virtual void finalize           () { }
            virtual void halt               ();

            //virtual void update             (float  real_frame_delta) { }
            virtual void render             (Rasterizer & rasterizer);

            //virtual void got_focus          () { }
            //virtual void size_changed       () { }

            virtual void pointer_pressed    (const Pointer_Event & event);
            virtual void pointer_dragged    (const Pointer_Event & event);
            virtual void pointer_released   (const Pointer_Event & event);

		};
	}

#endif