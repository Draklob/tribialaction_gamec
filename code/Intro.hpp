/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel �ngel Torres                           *
*                                                                             *
*  Julio 2014  - Programaci�n para m�viles                                    *
*                                                                             *
*  Ingenier�a y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef INTRO_HEADER
#define INTRO_HEADER

    #include <View.hpp>
    #include <Image.hpp>
    #include <Timer.hpp>
    #include <Shared_Ptr.hpp>

    namespace tribalAction
    {

        using snack::Image;
        using snack::Timer;
        using snack::Rasterizer;

        class Intro : public snack::View
        {
        private:

            enum
            {
                FADING_IN, WAITING, FADING_OUT
            }
            state;

            Timer               timer;
            Shared_Ptr< Image > logo_EA;
			Shared_Ptr< Image > logo_Plat;
			float logo_width;
			float logo_heigth;
            int   logo_opacity;
			int   numIntro;

        private:

            void initialize ();
            void update     (float);
            void render     (Rasterizer & renderer);

        };

    }

#endif
