/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel �ngel Torres                           *
*                                                                             *
*  Julio 2014  - Programaci�n para m�viles                                    *
*                                                                             *
*  Ingenier�a y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "App_Main_Manager.hpp"

int main ()
{
	//Lanzamos el manager principal de la app
    tribalAction::App_Main_Manager app_main_manager;
    app_main_manager.start ();	

    return (0);
}
