/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Javier Barreiro Portela   // Miguel �ngel Torres                           *
 *                                                                             *
 *  Julio 2014  - Programaci�n para m�viles                                    *
 *                                                                             *
 *  Ingenier�a y desarrollo de videojuegos                                     *
 *																			   *
 *  jbarreiro.23@gmail.com                                                     *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Intro.hpp"
#include <Display.hpp>
#include "App_Main_Manager.hpp"

#include "SingletonManagers.hpp"


using namespace snack;

namespace tribalAction
{

    void Intro::initialize ()
    {    
		state = FADING_IN;
        
		logo_EA	= Image::instance_of (  SingletonManagers::getInstance().app_main_manager->path + "Intro1.jpg" );
		logo_Plat = Image::instance_of(SingletonManagers::getInstance().app_main_manager->path + "Intro2.jpg");
		logo_width	= SingletonManagers::getInstance().app_main_manager->screenSizef.x();
		logo_heigth	= SingletonManagers::getInstance().app_main_manager->screenSizef.y();
		
        logo_opacity = 0;
		numIntro = 0;

        set_frames_per_second (25);
    }

    void Intro::update (float)
    {
        switch (state)
        {
            case FADING_IN:
            {
                logo_opacity += 10;

                if (logo_opacity > 255)
                {
                    logo_opacity = 255;
                    state        = WAITING;
                    timer.reset ();                 // Se inicializa el timer para cronometrar
                }                                   // la espera del logo

                break;
            }

            case WAITING:
            {
                if (timer.elapsed_seconds () > 1)
                {
                    state = FADING_OUT;
                }

                break;
            }

            case FADING_OUT:
            {
                logo_opacity -= 10;

                if (logo_opacity < 0)
                {
                    logo_opacity = 0;
					// Cuando termina todas las intros, cambiamos de esta view a la siguiente, 
					//en este caso la del manager del juego Game1
					if (numIntro == 1 )
						SingletonManagers::getInstance().app_main_manager->changeState( App_Main_Manager::GAME1 ); 
					else
					{
						state = FADING_IN;
						numIntro++;
					}
                }

                break;
            }
        }
    }

    void Intro::render (Rasterizer & renderer)
    {
        renderer.clear     (black);
        renderer.set_alpha (logo_opacity);

		if (numIntro == 0)
		{
			renderer.blit_image
				(
				*logo_EA, 0.f, 0.f, logo_width, logo_heigth
				);
		}
		if (numIntro == 1)
		{
			renderer.blit_image
				(
				*logo_Plat, 0.f, 0.f, logo_width, logo_heigth
				);
		}
    }

}
