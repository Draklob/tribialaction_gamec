/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel �ngel Torres                           *
*                                                                             *
*  Julio 2014  - Programaci�n para m�viles                                    *
*                                                                             *
*  Ingenier�a y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef SINGLETON_MANAGERS_HEADER
#define SINGLETON_MANAGERS_HEADER

	//Para manejar punteros inteligentes
	#include <Shared_Ptr.hpp>

	//Para incluir el vector de tendencias.
	#include <vector>

	//Las clases a la que queremos hacer referencia
	#include "App_Main_Manager.hpp"
	#include "Game1Manager.hpp"
	#include "Game1Gameplay.hpp"
	
    namespace tribalAction
    {

		using toolkit::Shared_Ptr;
		
        class SingletonManagers 
        {

		public:
			static SingletonManagers& getInstance()
			{
				static SingletonManagers instance;
				return instance;
			}
		private:
			SingletonManagers() {}
			SingletonManagers(SingletonManagers const&);// Don't Implement.
			void operator=(SingletonManagers const&);// Don't implement
 
		public:
			//Referencia al manager principal de la app
			App_Main_Manager* app_main_manager;

			//Referencia al manager principal del juego Game1
			game1::Game1Manager* game1Manager;

			// Referencia al gameplay principal del juego
			game1::Game1Gameplay* game1Gameplay;
			
		};
		

		/*class SingletonManagers 
		{
		private:
			static SingletonManagers* pInstance_;
			// Delete the singleton instance
			static void DestroySingleton()
			{
				if(pInstance_ != NULL) delete pInstance_;
			}
			// Private to ensure single instance
			SingletonManagers(){};
			SingletonManagers(const SingletonManagers& s){};
		public:
			virtual ~SingletonManagers(){};

			//Referencia al manager principal
			App_Main_Manager* app_main_manager;

			static SingletonManagers& getInstance() 
			{
				if(pInstance_ == NULL)
				{
					pInstance_ = new SingletonManagers();
					atexit(&DestroySingleton);    // At exit, destroy the singleton
				}
				return *pInstance_;
			}
		};*/
		


 
    }

#endif
