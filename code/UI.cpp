/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel �ngel Torres                           *
*                                                                             *
*  Julio 2014  - Programaci�n para m�viles                                    *
*                                                                             *
*  Ingenier�a y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "UI.hpp"

using namespace snack;
using namespace toolkit;


namespace tribalAction
{

	void UI::initialize()
	{
		
	}

	void UI::pointer_pressed (const Pointer_Event & event)
	{
		//Guardamos la posici�n del puntero
		pointerLocation = Vector2f((float)event.location_x, (float)event.location_y);
	}

	void UI::pointer_dragged (const Pointer_Event & event)
	{
		//Guardamos la posici�n del puntero
		pointerLocation = Vector2f((float)event.location_x, (float)event.location_y);
	}

	void UI::pointer_released (const Pointer_Event & event)
	{
		//Guardamos la posici�n del puntero
		pointerLocation = Vector2f((float)event.location_x, (float)event.location_y);
	}

	void UI::render (Rasterizer & rasterizer)
	{
	}

	void UI::halt()
	{
	}

}
	