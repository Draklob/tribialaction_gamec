/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Javier Barreiro Portela   // Miguel Ángel Torres                           *
*                                                                             *
*  Julio 2014  - Programación para móviles                                    *
*                                                                             *
*  Ingeniería y desarrollo de videojuegos                                     *
*																			   *
*  jbarreiro.23@gmail.com                                                     *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef APP_MAIN_MANAGER_HEADER
#define APP_MAIN_MANAGER_HEADER

    #include <View.hpp>
    #include <Image.hpp>
    #include <Shared_Ptr.hpp>

	#include "Game1Manager.hpp"

	//Para cargar una fuente para toda la app
	#include "Vector_Font.hpp"

	//Para cargar los textos según el idioma de la app
	#include <map>

    namespace tribalAction
    {
		//Incluimos una declaración adelantada de las clases que instanciamos 
		//desde el gestor principal de la app
		class Intro;
		class Game1Manager;

        using snack::Image;
        using snack::Rasterizer;
		using snack::Vector_Font;

		using std::string;
		using std::map;

        class App_Main_Manager : public snack::View
        {
			
		//Propiedades públicas
		public:

			//Posibles estados de la aplicación
			enum AppStates { NONE, INTRO, GAME1 } appState;

			//Para castear el tamaño de pantalla de int ( pixels ) a float
			Vector2f screenSizef;

			//Diferentes dispositivos con diferentes tamaños de pantalla
			enum DeviceType{ HIGH, MEDIUM, LOW } deviceType;

			//El path para buscar los recursos según el tamaño del dispositivo
			string path;

			//El factor de escala con respecto al diseño ( 1080 * 1920 )
			Vector2f designScaleFactor;

			//Lo que habrá que escalar las imágenes para ajustarlas al tamaño definitivo
			//partiendo del que más se aproxime de los disponibles
			float assetsScaleFactor;

			/* FONTS */
			//La fuente para toda la aplicación
			Shared_Ptr< Vector_Font > font_big;
			Shared_Ptr< Vector_Font > font_medium;
			Shared_Ptr< Vector_Font > font_small;
			Shared_Ptr< Vector_Font > font_micro;

			//Nos da los tres tamaños base de fuente para cada dispositivo
			struct Font_Size{
				int big;
				int medium;
				int small;
				int micro;
			} fontSize;

			/* IDIOMS */
			//El mapa para obtener los textos en el idioma seleccionado
			map<string, string> texts_map;

			//El idioma seleccionado
			enum { ENGLISH, SPANISH } idiom;

		//Propiedades privadas
        private:

			//Puntero a la intro
			Shared_Ptr< Intro > intro;

			//Referencia al gameManager del namespace game1
            Shared_Ptr<game1::Game1Manager> game1Manager;

		//Métodos públicos
		public:

			//Método que cambia el estado de la aplicación
			void changeState( const AppStates newState );
			
		//Métodos privados
        private:

			//Método que virtuales de View que se sobreescriben
            void initialize	();
			void halt		();
		
			//Métodos que lanzan las diferentes aplicaciones incluidad dentro de la aplicación global
			void start_Intro();
			void start_Game1();

			//Método que carga los textos según el idioma
			void load_texts();

			/* EXTERN FUNCTION */
			template< class OUTPUT_CONTAINER_TYPE >
			OUTPUT_CONTAINER_TYPE & convert_utf8_to_string (const char * input_begin, size_t input_size, OUTPUT_CONTAINER_TYPE & output);

        };

    }

#endif
